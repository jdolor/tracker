/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 70);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */
/***/ (function(module, exports) {

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = Object.create(options.computed || null)
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
    options.computed = computed
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

var routes = [{ path: '/about', component: __webpack_require__(53) }, { path: '/contact-us', component: __webpack_require__(55) }, { path: '/services', component: __webpack_require__(56) }, { path: '/tracking-number', component: __webpack_require__(57) }, { path: '/mobile-number', component: __webpack_require__(54) }];

var router = new VueRouter({
    routes: routes // short for `routes: routes`
});

router.replace({ path: '/tracking-number', redirect: '/' });
// load components
Vue.component('modal', __webpack_require__(52));

var app = new Vue({
    router: router,
    el: '.app',
    data: {
        appTitle: 'WYC Group'
    },

    methods: {
        closeMenu: function closeMenu(title) {
            this.appTitle = title;
            hideDrawer();
        }
    },

    created: function created() {}
});

/***/ }),
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        id: { default: 'wyc-modal' },
        size: { default: 'md' }
    },

    computed: {
        modalSize: function modalSize() {
            return 'modal-' + this.size;
        }
    }
});

/***/ }),
/* 37 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),
/* 38 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({});

// 	$('#btnGoToMobilePage').click(function(){
//     if(storage.getItem('user_mobile_number').length > 0)
//     {
//         $('#mobile_number').val($('#user_mobile_number').html());
//         $('#btnMobileSubmit').click();
//     }
// })

// $('#btnMobileSubmit').click(function(){

// 	contact_number = $('#mobile_number').val();
// 	group = $('#package-panel-group');


//     // alert(storage.getItem('user_mobile_number'));
//     // 9178429679
//     // seong-ho
//     // won
//     // male
// 	if(contact_number.length > 6) //wrote 10 characters or tel num
// 	{
// 		// contact_number = '+639155705010';
// 		//last 10 characters
// 		contact_number = contact_number.substr(contact_number.length - 10);
//         // 9155705010
// 		$.ajax({
//         	url: window.base_url+'api/get-packages-number',
//         	data:{number:contact_number},
//         	dataType:'json',
//         	type:'POST',
//             beforeSend:function(){
//                 window.data_packages = null;
//                 window.clients = [];
//             },
//         	success:function(data){
//                 storage.setItem('user_mobile_number', contact_number);

//                 window.setNameAndNumber();
//                 if(data[0] == null)
//         		{
//                     $.snackbar({content: 'No existing number'});
//         		}else{
//                     if(data.length > 1)
//                     {
//                         //many accounts
//                         window.data_packages = data;
//                         for(j=0; j<data.length; j++)
//                         {
//                             window.clients.push(data[j].client);
//                         }
//                         $('#btnMobileSubmit').attr('href','#asking_credentials_container');
//                         $('#btnMobileSubmit').tab('show');
//                         return;
//                     }else
//                     {
//                         // get details
//                         window.processing_client = data[0].client_id;
//                         window.processing_client_name = data[0].client.client_lname+', '+data[0].client.client_fname;

//                         storage.setItem('user_full_name', window.processing_client_name);
//                         window.setNameAndNumber();

//                         $.ajax({
//                             url:window.base_url+'api/get-packages-client',
//                             data:{client_id:processing_client},
//                             dataType:'json',
//                             type:'POST',
//                             beforeSend:function(){
//                                 window.processing_client_leader = false;
//                             },
//                             success:function(sdata){
//                                 compilePanelsIndividual(sdata);
//                                 if(sdata.is_leader)
//                                 {
//                                     window.processing_client_leader = sdata.is_leader;

//                                     $('#btnMobileSubmit').attr('href','#choices_container');
//                                     $('#btnMobileSubmit').tab('show');
//                                 }else
//                                 {
//                                     $('#btnMobileSubmit').attr('href','#package_container');
//                                     $('#btnMobileSubmit').tab('show');
//                                 }
//                             },
//                             error:function(error){
//                                 // alert('No existing number');
//                             }
//                         })

//                     }
//         		}
//         	},
//         	error:function(error){
//                 // $.snackbar({content: 'No existing number'});
//         	}
// 		})
// 	}else
//     {
//         $.snackbar({content: 'Minimum of 7 Characters.'});
//     }
// })


// $('#btnDetailsSubmit').click(function(){
//     client_fname = $('#fname').val();
//     client_lname = $('#lname').val();
//     client_gender = $("input[name=gender]:checked").val();


//     for(i=0; i< window.clients.length; i++)
//     {
//         parsed = window.clients[i];
//         // debugger;
//         if(
//             !parsed
//         ){
//             continue;
//         }

//         if(
//             (
//                 (
//                 client_fname.toLowerCase() == (window.clients[i].client_fname).toLowerCase().trim()
//                     &&
//                 client_lname.toLowerCase() == (window.clients[i].client_lname).toLowerCase().trim()
//                 )
//             ||
//                 (
//                 client_lname.toLowerCase() == (window.clients[i].client_fname).toLowerCase().trim()
//                     &&
//                 client_fname.toLowerCase() == (window.clients[i].client_lname).toLowerCase().trim()
//                 )
//             )
//             &&
//             (client_gender.toLowerCase() == (window.clients[i].client_gender).toLowerCase().trim())
//         ){
//             window.processing_client = window.clients[i].client_id;
//             window.processing_client_name = window.clients[i].client_lname+', '+window.clients[i].client_fname;

//             $.ajax({
//                 url:window.base_url+'api/get-packages-client',
//                 data:{client_id:window.processing_client},
//                 dataType:'json',
//                 type:'POST',
//                 success:function(sdata){
//                     compilePanelsIndividual(sdata);
//                     if(sdata.is_leader)
//                     {
//                         window.processing_client_leader = sdata.is_leader;

//                         compilePanels(window.data_packages);
//                         $('#btnMobileSubmit').attr('href','#choices_container');
//                         $('#btnMobileSubmit').tab('show');
//                     }else
//                     {
//                         $('#btnMobileSubmit').attr('href','#package_container');
//                         $('#btnMobileSubmit').tab('show');
//                     }
//                 },
//                 error:function(error){
//                 }
//             })

//             return;
//         }
//     }

//     $.snackbar({content: 'No records found!'});
// })

/***/ }),
/* 39 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),
/* 40 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            services: [{
                id: 1,
                title: 'ACR-ICARD',
                description: 'An ACR-ICARD is a microchip based, credit card-sized, identification card issued to all registered aliens whose stay in the Philippines has exceeded fifty-nine (59) days.'
            }, {
                id: 2,
                title: 'Alien Employment Permit (AEP)',
                description: 'The ARP is a Departmentof Justice - Bureau of Immigration (BI) initiative based on three-phase project. The First Phase of the Project involves the set-up and implementation of Biometric data-capturing devices. The Second and Third Phase shall implement the use of servers and data analyzing devices.'
            }, {
                id: 3,
                title: 'BI-Certification',
                description: 'The BI-Certification is an individual certifying that he/she is not in any of derogatory database, list or record of the Bureau.'
            }, {
                id: 4,
                title: 'Black List',
                description: 'The Black List is a list or register of entities or people, for one reason or another, are being denied a particular privilege, service, mobility, access or recognition.'
            }, {
                id: 5,
                title: 'Bureau of Investment (BOI)',
                description: 'Bureau of Investments The Philippine Board of Investments (BOI), an attached agency of Department of Trade and Industry (DTI), is the lead government agency responsible for the promotion of investments in the Philippines.'
            }, {
                id: 6,
                title: 'Bureau of Quarantine',
                description: 'Bureau of Quarantine (BOQ) is the health authority and a line bureau of the Department of Health (DOH). BOQ is mandated to ensure security against the introduction and spread of infectious diseases, emerging diseases and public health emergencies of international concern (PHEIC).'
            }, {
                id: 7,
                title: 'Ceza Working Visa',
                description: 'The Cagayan Economic Zone Authority (CEZA) is a government owned and controlled corporation that was created by virtue of Republic Act 7922, otherwise known as the “Cagayan Special Economic Zone Act of 1995″.'
            }, {
                id: 8,
                title: 'Company Registration',
                description: 'The company name must be officially approved by the Registrar of Companies. On applying to the Registrar for the approval of a name, it is recommended that two or three possible names ending with the word “limited” be submitted as this may avoid unnecessary delays.'
            }, {
                id: 9,
                title: 'Department of Justice (DOJ)',
                description: 'The Department of Justice (DOJ) is the judicial department of the Philippine government responsible for upholding the rule of law in the Philippines. It is the government\'s principal law agency, serving as its legal counsel and prosecution arm.'
            }, {
                id: 10,
                title: 'Embassy Services',
                description: 'The Embassy Services is help and advice provided by the diplomatic agents of a country to citizens of that country who are living or traveling overseas.'
            }, {
                id: 11,
                title: 'Emigration Clearance Certificate (ECC)',
                description: 'The Emigration Clearance Certificate (ECC), although not technically a visa is grouped with them because it is often referred to by foreign nationals as an exit visa, and frankly it has that kind of power, without it you don’t get to leave the country when you planned.'
            }, {
                id: 12,
                title: 'NSO Certificates',
                description: 'The National Statistics Office (NSO) is the primary statistical agency of the government mandated to collect, compile, classify, produce, publish, and disseminate general-purpose statistics as provided for in Commonwealth Act No. 591.'
            }, {
                id: 13,
                title: 'Re-stamping of Visa',
                description: 'Foreign nationals with visas that are not fully implemented or for reasons of lost or damaged passports.'
            }, {
                id: 14,
                title: 'Special Retirement Residence Visa (SRRV)',
                description: 'The Information Guide to the Special Resident Retiree\'s Visa (SRRV) is designed as a detailed reference to assist retiree applicants in understanding the different SRRV Options, the application requirements and process, and the obligations of being a PRA retiree member.'
            }, {
                id: 15,
                title: 'Student Visa (9F)',
                description: 'The applicant for student visa, having means sufficient for education and support in the Philippines, must be at least eighteen (18) years old and seeks to enter the Philippines temporarily and solely for the purpose of taking up a course of study higher than high school at a university, seminary, academy, college or school, approved for foreign students by the Commissioner of Immigration.'
            }, {
                id: 16,
                title: 'The National Bureau of Investigation (NBI)',
                description: 'The National Bureau of Investigation (NBI) is an agency of the Philippine government under the Department of Justice, responsible for handling and solving major high-profile cases that are in the interest of the nation.'
            }, {
                id: 17,
                title: 'Tourist Visa',
                description: 'A Tourist Visa is a conditional authorization granted by a country (typically to a foreigner) to enter and temporarily remain within, or to leave that country. Visas typically include limits on the duration of the foreigner\'s stay, territory within the country they may enter, the dates they may enter, or the number of permitted visits.'
            }, {
                id: 18,
                title: 'Visa Status Verification',
                description: 'If you have applied or petitioned for an immigration benefit, you can check the status of your case online.'
            }, {
                id: 19,
                title: 'Waiver for Exclusion Ground',
                description: 'Under Section 29(a) (12) of the Commonwealth Act No. 613 or the Philippine Immigration Act of 1940, children below fifteen (15) years of age who are unaccompanied by or not coming to a parent, are classified as excludable. In order for them to be admitted into the Philippines, they must secure a Waiver of Exclusion Ground (WEG) from the Bureau of Immigration, upon proper coordination with the Department of Foreign Affairs (DFA).'
            }, {
                id: 20,
                title: 'Working Visa (9G)',
                description: 'A foreigner who desires to work in the Philippines must secure a 9g or work visa from the Bureau of Immigration. The process will be a change of Status to Pre-Arranged Employee under Section 9(G) of the Philippine Immigration Act of 1940, as amended.'
            }, {
                id: 21,
                title: 'Other Services',
                description: 'We assist Other Services like Affidavit, Lost ICard, and Tin Card.'
            }]
        };
    },


    methods: {
        showDescription: function showDescription(id) {
            var service = this.services.filter(function (obj) {
                return obj.id === id;
            });
            if (service) {
                service = service[0];
                $('.modal-title').html(service.title);
                $('.modal-body').html(service.description);
                $('#serviceModal').modal('toggle');
            }
        }
    }

    // $(document).ready(()=>{
    //     $('.modal-body').scroll(()=>{
    //         clearTimeout($.data(this, 'scrollTimer'));
    //         $('.modal-header').css('box-shadow','0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2)');
    //         $('.modal-footer').css('box-shadow','0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2)');
    //         $.data(this, 'scrollTimer', setTimeout(function() {
    //             // do something
    //             $('.modal-header').css('box-shadow','none');
    //             $('.modal-footer').css('box-shadow','none');
    //         }, 250));
    //     })
    // })

});

/***/ }),
/* 41 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	data: function data() {
		return {
			form: new Form({
				package: ''
			}, { baseURL: window.base_url })
		};
	},


	methods: {
		submit: function submit() {
			this.form.submit('post', window.tracking_number_url).then(function (result) {
				console.log(result);
			}).catch($.noop);
		}
	}

	// end

	// $('#btnTrackingSubmit').click(function(){

	//     tracking_number = $('#tracking_number').val();
	//     $.ajax({
	//         url:window.base_url+'api/track/'+tracking_number,
	//         type: 'GET',
	//         dataType: 'json',
	//         beforeSend(){
	//             $('#tracking_table').html('');
	//         },
	//         success:function(data)
	//         {
	//             td = '';
	//             if(data)
	//             {

	//                 //group package
	//                 if(tracking_number.toUpperCase().indexOf('GL') > -1)
	//                 {

	//                     package_cost = 0;
	//                     td += "<table class='table table-bordered'>\
	//                         <thead>\
	//                             <tr>\
	//                                 <th>Name</th>\
	//                                 <th>Client #</th>\
	//                                 <th>Total Service Cost</th>\
	//                             </tr>\
	//                         </thead>\
	//                         <tbody id=''>";

	//                     for(var i=0; i< data.services.length; i++)
	//                     {
	//                         td+= '<tr id="seeDetails" data-group="'+data.services[i].group_id+'" data-client="'+data.services[i].client_id+'"  >';
	//                             td+= '<td>'+toTitleCase(data.services[i].client_lname)+', '+toTitleCase(data.services[i].client_fname)+'</td>';
	//                             td+= '<td>'+data.services[i].client_id+'</td>';
	//                             td+= '<td>'+numberWithCommas(data.services[i].total_cost)+'</td>';
	//                         td+= '</tr>';
	//                         total_cost = '0'+data.services[i].total_cost;
	//                     }

	//                     td+=    "</tbody>\
	//                     </table>";

	//                     $('#tracking_table').html(td);
	//                     $('#client_name').html(tracking_number);
	//                     $('#client_id').html(data.group);

	//                     package_cost = parseFloat(data.cost);

	//                     $('#total_package_cost').html(numberWithCommas(package_cost));
	//                     $('#initial_deposit').html(numberWithCommas(data.deposit));
	//                     $('#total_payment').html(numberWithCommas(data.payment));
	//                     $('#total_refund').html(numberWithCommas(data.refund));
	//                     $('#total_discount').html(numberWithCommas(data.discount));
	//                     $('#available_balance').html(numberWithCommas(data.balance));

	//                 }else //member package or normal package
	//                 {

	//                     package_cost = 0;
	//                     td += "<table class='table table-bordered'>\
	//                                 <thead>\
	//                                     <tr>\
	//                                         <th>Service Status</th>\
	//                                         <th>Date Created</th>\
	//                                         <th>Service Detail</th>\
	//                                         <th>Service Cost</th>\
	//                                         <th>Additional Charge</th>\
	//                                         <th>Service Fee</th>\
	//                                         <th>Total Cost</th>\
	//                                     </tr>\
	//                                 </thead>\
	//                                 <tbody >";

	//                     for(var i=0; i< data.list.length; i++)
	//                     {
	//                         td+= '<tr id="seeLogs" data-id="'+data.list[i].id+'">';
	//                             td+= '<td>'+data.list[i].status+'</td>';
	//                             td+= '<td>'+data.list[i].service_date+'</td>';
	//                             td+= '<td>'+data.list[i].detail+'</td>';
	//                             td+= '<td>'+numberWithCommas(data.list[i].amount)+'</td>';
	//                             td+= '<td>'+numberWithCommas(data.list[i].charge2)+'</td>';
	//                             td+= '<td>'+numberWithCommas(data.list[i].charge)+'</td>';
	//                             td+= '<td>'+ numberWithCommas(parseFloat(data.list[i].charge)+parseFloat(data.list[i].amount)) +'</td>';
	//                         td+= '</tr>';
	//                         package_cost += (parseFloat(data.list[i].charge)+parseFloat(data.list[i].amount));
	//                     }
	//                      td+=    "</tbody>\
	//                     </table>";
	//                     $('#tracking_table').html(td);
	//                     $('#client_name').html(data.client.client_lname+', '+data.client.client_fname);
	//                     $('#client_id').html(data.client.client_id);

	//                     package_cost = data.cost;
	//                     $('#total_package_cost').html(numberWithCommas(package_cost));
	//                     $('#initial_deposit').html(numberWithCommas(data.deposit));
	//                     $('#total_payment').html(numberWithCommas(data.payment));
	//                     $('#total_refund').html(numberWithCommas(data.refund));
	//                     $('#total_discount').html(numberWithCommas(data.discount));
	//                     $('#available_balance').html(numberWithCommas(data.balance));
	//                 }

	//                 $('#btnTrackingSubmit').tab('show');
	//             }

	//         },
	//         error:function(data)
	//         {
	//         }
	//         // http://topwyc.com/api/track/4897496
	//     })
	// })

	// $('body').on('click','[id="seeDetails"]',function(){
	//     post_data = {};
	//      post_data.group_id = $(this).data('group')
	//      post_data.client_id = $(this).data('client')
	//      $.ajax({
	//         url:base_url+'tracking/member-service/',
	//         data:post_data,
	//         type:'POST',
	//         dataType:'json',
	//         success:function(data)
	//         {
	//             td = '';
	//             if(data.status == 'success')
	//             {
	//                 for(i=0; i< data.nodes.length; i++)
	//                 {
	//                     td += '<tr>';
	//                     td += '<td>' + data.nodes[i].serviceDate + '</td>';
	//                     td += '<td>' + data.nodes[i].tracking + '</td>';
	//                     td += '<td>' + data.nodes[i].detail + '</td>';
	//                     td += '<td>' + numberWithCommas(data.nodes[i].amount )+ '/Cost</td>';
	//                     td += '<td>' + numberWithCommas(data.nodes[i].charge )+ '/SF</td>';
	//                     td += '<td>' + numberWithCommas(data.nodes[i].charge2 )+ '/AC</td>';
	//                     td += '</tr>';
	//                 }
	//             }
	//             $('#groupModal tbody').html(td);
	//             $('#groupModal').modal('toggle');
	//         }
	//      })

	// })

	// $('#btnBackTracking').click(function(){
	//     showTab('#tracking_container');
	// })

});

/***/ }),
/* 42 */,
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(36),
  /* template */
  __webpack_require__(63),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Users/joshuadolor/Documents/Projects/Tracker/resources/js/components/Modal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Modal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-fb34d4ca", Component.options)
  } else {
    hotAPI.reload("data-v-fb34d4ca", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(37),
  /* template */
  __webpack_require__(58),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Users/joshuadolor/Documents/Projects/Tracker/resources/js/pages/About.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] About.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0e9c02eb", Component.options)
  } else {
    hotAPI.reload("data-v-0e9c02eb", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(38),
  /* template */
  __webpack_require__(60),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Users/joshuadolor/Documents/Projects/Tracker/resources/js/pages/ContactNumber.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ContactNumber.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-25632ee7", Component.options)
  } else {
    hotAPI.reload("data-v-25632ee7", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(39),
  /* template */
  __webpack_require__(59),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Users/joshuadolor/Documents/Projects/Tracker/resources/js/pages/ContactUs.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ContactUs.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-229662bc", Component.options)
  } else {
    hotAPI.reload("data-v-229662bc", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(40),
  /* template */
  __webpack_require__(61),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Users/joshuadolor/Documents/Projects/Tracker/resources/js/pages/Services.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Services.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-374b9af0", Component.options)
  } else {
    hotAPI.reload("data-v-374b9af0", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(41),
  /* template */
  __webpack_require__(62),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Users/joshuadolor/Documents/Projects/Tracker/resources/js/pages/TrackNumber.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] TrackNumber.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-727b485c", Component.options)
  } else {
    hotAPI.reload("data-v-727b485c", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _vm._m(0)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    attrs: {
      "id": "about-page"
    }
  }, [_c('table', {
    staticClass: "table"
  }, [_c('tr', {
    attrs: {
      "id": "transparency"
    }
  }, [_c('td', [_c('h3', [_vm._v("Transparency")]), _vm._v(" "), _c('p', [_vm._v("We undertake to be transparent in all our professional contacts. In practice, this means we will identify our client in proactive external professional\n\t            contacts and services offered for a client.")]), _vm._v(" "), _c('p', [_vm._v("Sometimes we establish coalitions on behalf of clients to work on specific issues. Whenever we do so, we undertake to be transparent about \n\t            the purpose of the coalition and principal funders.")])])]), _vm._v(" "), _c('tr', {
    attrs: {
      "id": "honesty"
    }
  }, [_c('td', [_c('h3', [_vm._v("Honesty")]), _vm._v(" "), _c('p', [_vm._v("We undertake not to disclose any confidential information provided to us in the course of our work for clients, unless explicity authorized to do\n\t            so or required by governments or other legal authorities. All WYC employees are contractually obligued to respect this undertaking on\n\t            all internal matters.")]), _vm._v(" "), _c('p', [_vm._v("We store and handle confidential information so as to ensure that only those who are working with the information have access to it.")]), _vm._v(" "), _c('p', [_vm._v("We advice our clients to uphold all these values in their own related to the advice we provide them.")])])]), _vm._v(" "), _c('tr', {
    attrs: {
      "id": "confidentiality"
    }
  }, [_c('td', [_c('h3', [_vm._v("Confidentiality")]), _vm._v(" "), _c('p', [_vm._v("We understand to be honest in all our professional dealings. We undertake never knowingly to spread false or misleading information and to take \n\t            reasonable care to avoid doing so inadvertently.")]), _vm._v(" "), _c('p', [_vm._v("We also ask our clients to be honest with us and not to request that we compromised our principles or the law.")]), _vm._v(" "), _c('p', [_vm._v("We take every possible step to avoid conflicts of interest.")])])]), _vm._v(" "), _c('tr', {
    attrs: {
      "id": "integrity"
    }
  }, [_c('td', [_c('h3', [_vm._v("Integrity")]), _vm._v(" "), _c('p', [_vm._v("We will only work for the organizations that are prepared to uphold the values of transparency and honesty in the course of our work for them.")]), _vm._v(" "), _c('p', [_vm._v("Sometimes our clients are involved with contentiuos issues. It is part of our job to provide communications assistance to those clients. \n\t            We undertake to represent those clients only in ways consistent with the values of honesty and transparency both in what they do and \n\t            in what we do for them.")])])])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-0e9c02eb", module.exports)
  }
}

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _vm._m(0)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-xs-12"
  }, [_c('h3', [_c('b', [_vm._v(" Address ")]), _c('br'), _c('small', [_vm._v(" U110 Balagtas St, Balagtas Villas Barangay 15 San Isidro Pasay City ")])]), _vm._v(" "), _c('h3', [_c('b', [_vm._v(" Contact ")]), _c('br'), _c('small', [_vm._v(" (02) 354 80 21 ")])]), _vm._v(" "), _c('h3', [_c('b', [_vm._v(" Operating hours")]), _vm._v(" "), _c('br'), _c('small', [_vm._v(" Monday - Friday (8am - 5pm)")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-229662bc", module.exports)
  }
}

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _vm._m(0)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-xs-12"
  }, [_c('h3', [_c('b', [_vm._v(" Address ")]), _c('br'), _c('small', [_vm._v(" U110 Balagtas St, Balagtas Villas Barangay 15 San Isidro Pasay City ")])]), _vm._v(" "), _c('h3', [_c('b', [_vm._v(" Contact ")]), _c('br'), _c('small', [_vm._v(" (02) 354 80 21 ")])]), _vm._v(" "), _c('h3', [_c('b', [_vm._v(" Operating hours")]), _vm._v(" "), _c('br'), _c('small', [_vm._v(" Monday - Friday (8am - 5pm)")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-25632ee7", module.exports)
  }
}

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    attrs: {
      "id": "services-pages"
    }
  }, [_vm._l((_vm.services), function(service) {
    return _c('button', {
      staticClass: "btn btn-list",
      on: {
        "click": function($event) {
          _vm.showDescription(service.id)
        }
      }
    }, [_vm._v("\n        " + _vm._s(service.title) + "\n    ")])
  }), _vm._v(" "), _c('modal', {
    attrs: {
      "id": "serviceModal"
    }
  }, [_c('template', {
    attrs: {
      "slot": "modal-title"
    },
    slot: "modal-title"
  }, [_vm._v("Modal Title")]), _vm._v(" "), _c('template', {
    attrs: {
      "slot": "modal-body"
    },
    slot: "modal-body"
  }, [_vm._v("Modal Title")])], 2)], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-374b9af0", module.exports)
  }
}

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-12"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "card"
  }, [_c('div', {
    staticClass: "card-body"
  }, [_c('div', {
    staticClass: "form-group form-group-lg  label-floating",
    class: {
      'has-error': _vm.form.errors.has('package')
    }
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Tracking Number")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.form.package),
      expression: "form.package"
    }],
    staticClass: "form-control",
    attrs: {
      "id": "trackingNumber"
    },
    domProps: {
      "value": (_vm.form.package)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.form.package = $event.target.value
      }
    }
  })]), _vm._v(" "), (_vm.form.errors.has('package')) ? _c('p', {
    staticClass: "text-danger help-block",
    domProps: {
      "textContent": _vm._s(_vm.form.errors.get('package'))
    }
  }) : _vm._e(), _vm._v(" "), _c('center', [_c('button', {
    staticClass: "text-center btn btn-lg btn-raised btn-primary",
    on: {
      "click": function($event) {
        _vm.submit()
      }
    }
  }, [_vm._v("Submit")])])], 1)])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('h3', [_vm._v("Tracking Number\n        \t"), _c('br'), _vm._v(" "), _c('small', [_vm._v("Monitor your package by entering the tracking number given by WYC Business Consultancy.")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-727b485c", module.exports)
  }
}

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "modal ",
    attrs: {
      "id": _vm.id,
      "tabindex": "-1",
      "role": "dialog",
      "aria-labelledby": "wyc-modal-label"
    }
  }, [_c('div', {
    staticClass: "modal-dialog",
    class: _vm.modalSize
  }, [_c('div', {
    staticClass: "modal-content animated fadeIn"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_c('h4', {
    staticClass: "modal-title",
    attrs: {
      "id": "wyc-modal-label"
    }
  }, [_vm._t("modal-title", [_vm._v("Modal Title")])], 2)]), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("modal-body", [_vm._v("Modal Body")])], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("modal-footer", [_c('button', {
    staticClass: "btn btn-primary",
    attrs: {
      "type": "button",
      "data-dismiss": "modal"
    }
  }, [_vm._v("Close")])])], 2)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-fb34d4ca", module.exports)
  }
}

/***/ }),
/* 64 */,
/* 65 */,
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(11);


/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgNTcxNDFiNDc3ZmU4ZDMwNjFiMWEiLCJ3ZWJwYWNrOi8vLy4vfi92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplci5qcyIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vL01vZGFsLnZ1ZSIsIndlYnBhY2s6Ly8vQWJvdXQudnVlIiwid2VicGFjazovLy9Db250YWN0TnVtYmVyLnZ1ZSIsIndlYnBhY2s6Ly8vQ29udGFjdFVzLnZ1ZSIsIndlYnBhY2s6Ly8vU2VydmljZXMudnVlIiwid2VicGFjazovLy9UcmFja051bWJlci52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL2NvbXBvbmVudHMvTW9kYWwudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy9wYWdlcy9BYm91dC52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3BhZ2VzL0NvbnRhY3ROdW1iZXIudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy9wYWdlcy9Db250YWN0VXMudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy9wYWdlcy9TZXJ2aWNlcy52dWUiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3BhZ2VzL1RyYWNrTnVtYmVyLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvcGFnZXMvQWJvdXQudnVlP2Y2YWYiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3BhZ2VzL0NvbnRhY3RVcy52dWU/OTgzYiIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvcGFnZXMvQ29udGFjdE51bWJlci52dWU/OTY4YSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvcGFnZXMvU2VydmljZXMudnVlPzJmZGEiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3BhZ2VzL1RyYWNrTnVtYmVyLnZ1ZT81ZDkwIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy9jb21wb25lbnRzL01vZGFsLnZ1ZT8zNzE0Il0sIm5hbWVzIjpbInJvdXRlcyIsInBhdGgiLCJjb21wb25lbnQiLCJyZXF1aXJlIiwicm91dGVyIiwiVnVlUm91dGVyIiwicmVwbGFjZSIsInJlZGlyZWN0IiwiVnVlIiwiYXBwIiwiZWwiLCJkYXRhIiwiYXBwVGl0bGUiLCJtZXRob2RzIiwiY2xvc2VNZW51IiwidGl0bGUiLCJoaWRlRHJhd2VyIiwiY3JlYXRlZCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ2hFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQyxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsREEsSUFBTUEsU0FBUyxDQUNYLEVBQUVDLE1BQU0sUUFBUixFQUFrQkMsV0FBVyxtQkFBQUMsQ0FBUSxFQUFSLENBQTdCLEVBRFcsRUFFWCxFQUFFRixNQUFNLGFBQVIsRUFBdUJDLFdBQVcsbUJBQUFDLENBQVEsRUFBUixDQUFsQyxFQUZXLEVBR1gsRUFBRUYsTUFBTSxXQUFSLEVBQXFCQyxXQUFXLG1CQUFBQyxDQUFRLEVBQVIsQ0FBaEMsRUFIVyxFQUlYLEVBQUVGLE1BQU0sa0JBQVIsRUFBNEJDLFdBQVcsbUJBQUFDLENBQVEsRUFBUixDQUF2QyxFQUpXLEVBS1gsRUFBRUYsTUFBTSxnQkFBUixFQUEwQkMsV0FBVyxtQkFBQUMsQ0FBUSxFQUFSLENBQXJDLEVBTFcsQ0FBZjs7QUFRQSxJQUFNQyxTQUFTLElBQUlDLFNBQUosQ0FBYztBQUN6Qkwsa0JBRHlCLENBQ2xCO0FBRGtCLENBQWQsQ0FBZjs7QUFJQUksT0FBT0UsT0FBUCxDQUFlLEVBQUVMLE1BQU0sa0JBQVIsRUFBNEJNLFVBQVUsR0FBdEMsRUFBZjtBQUNBO0FBQ0FDLElBQUlOLFNBQUosQ0FBYyxPQUFkLEVBQXVCLG1CQUFBQyxDQUFRLEVBQVIsQ0FBdkI7O0FBRUEsSUFBSU0sTUFBTSxJQUFJRCxHQUFKLENBQVE7QUFDZEosa0JBRGM7QUFFZE0sUUFBSSxNQUZVO0FBR2RDLFVBQUs7QUFDREMsa0JBQVU7QUFEVCxLQUhTOztBQU9kQyxhQUFRO0FBQ0pDLGlCQURJLHFCQUNNQyxLQUROLEVBQ2E7QUFDYixpQkFBS0gsUUFBTCxHQUFnQkcsS0FBaEI7QUFDQUM7QUFDSDtBQUpHLEtBUE07O0FBY2RDLFdBZGMscUJBY0wsQ0FFUjtBQWhCYSxDQUFSLENBQVYsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ09BOzt1QkFHQTt5QkFHQTtBQUpBOzs7d0NBTUE7bUNBQ0E7QUFFQTtBQUpBO0FBTkEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMyQkEsbUU7Ozs7Ozs7Ozs7Ozs7OztBQzFDQTs7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDNUtBLG1FOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNJQTswQkFFQTs7QUFFQTtvQkFHQTt1QkFDQTs2QkFFQTtBQUpBLGFBREE7b0JBT0E7dUJBQ0E7NkJBRUE7QUFKQTtvQkFNQTt1QkFDQTs2QkFFQTtBQUpBO29CQU1BO3VCQUNBOzZCQUVBO0FBSkE7b0JBTUE7dUJBQ0E7NkJBRUE7QUFKQTtvQkFNQTt1QkFDQTs2QkFFQTtBQUpBO29CQU1BO3VCQUNBOzZCQUVBO0FBSkE7b0JBTUE7dUJBQ0E7NkJBRUE7QUFKQTtvQkFNQTt1QkFDQTs2QkFFQTtBQUpBO29CQU1BO3VCQUNBOzZCQUVBO0FBSkE7b0JBTUE7dUJBQ0E7NkJBRUE7QUFKQTtvQkFNQTt1QkFDQTs2QkFFQTtBQUpBO29CQU1BO3VCQUNBOzZCQUVBO0FBSkE7b0JBTUE7dUJBQ0E7NkJBRUE7QUFKQTtvQkFNQTt1QkFDQTs2QkFFQTtBQUpBO29CQU1BO3VCQUNBOzZCQUVBO0FBSkE7b0JBTUE7dUJBQ0E7NkJBRUE7QUFKQTtvQkFNQTt1QkFDQTs2QkFFQTtBQUpBO29CQU1BO3VCQUNBOzZCQUVBO0FBSkE7b0JBTUE7dUJBQ0E7NkJBRUE7QUFKQTtvQkFNQTt1QkFDQTs2QkFJQTtBQU5BO0FBdkdBO0FBK0dBOzs7O3NEQUVBOzs7QUFDQTtnQkFDQSxTQUNBO2tDQUNBOytDQUNBOzhDQUNBO3lDQUNBO0FBQ0E7QUFFQTtBQVhBOztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUExSUEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDUUE7dUJBRUE7OzthQUdBO0FBREEsd0JBR0E7QUFKQTtBQU1BOzs7OzRCQUVBO1FBQ0EsMkJBQ0EsNENBQ0E7Z0JBQ0E7QUFDQSxjQUNBO0FBRUE7QUFUQTs7QUFXQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQTNLQSxHOzs7Ozs7Ozs7Ozs7Ozs7O0FDdkJBO0FBQ0E7QUFDQSx3QkFBcUo7QUFDcko7QUFDQSx3QkFBc0c7QUFDdEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7QUMzQkE7QUFDQTtBQUNBLHdCQUFxSjtBQUNySjtBQUNBLHdCQUFzRztBQUN0RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7OztBQzNCQTtBQUNBO0FBQ0Esd0JBQXFKO0FBQ3JKO0FBQ0Esd0JBQXNHO0FBQ3RHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7O0FDM0JBO0FBQ0E7QUFDQSx3QkFBcUo7QUFDcko7QUFDQSx3QkFBc0c7QUFDdEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFLGlEQUFpRCxJQUFJO0FBQ3BJLG1DQUFtQzs7QUFFbkM7QUFDQSxZQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7O0FBRUQ7Ozs7Ozs7QUMzQkE7QUFDQTtBQUNBLHdCQUFxSjtBQUNySjtBQUNBLHdCQUFzRztBQUN0RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsaURBQWlELElBQUk7QUFDcEksbUNBQW1DOztBQUVuQztBQUNBLFlBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7OztBQzNCQTtBQUNBO0FBQ0Esd0JBQXFKO0FBQ3JKO0FBQ0Esd0JBQXNHO0FBQ3RHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtFQUErRSxpREFBaUQsSUFBSTtBQUNwSSxtQ0FBbUM7O0FBRW5DO0FBQ0EsWUFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7O0FDM0JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQSxDQUFDLCtCQUErQixhQUFhLDBCQUEwQjtBQUN2RTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7OztBQ2pDQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0EsQ0FBQywrQkFBK0IsYUFBYSwwQkFBMEI7QUFDdkU7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7O0FDYkEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBLENBQUMsK0JBQStCLGFBQWEsMEJBQTBCO0FBQ3ZFO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7OztBQ2JBLGdCQUFnQixtQkFBbUIsYUFBYSwwQkFBMEI7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxHQUFHO0FBQ0gsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7OztBQ3BDQSxnQkFBZ0IsbUJBQW1CLGFBQWEsMEJBQTBCO0FBQzFFO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDLCtCQUErQixhQUFhLDBCQUEwQjtBQUN2RTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7QUN4REEsZ0JBQWdCLG1CQUFtQixhQUFhLDBCQUEwQjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQyIsImZpbGUiOiJqcy9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gNzApO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIDU3MTQxYjQ3N2ZlOGQzMDYxYjFhIiwiLy8gdGhpcyBtb2R1bGUgaXMgYSBydW50aW1lIHV0aWxpdHkgZm9yIGNsZWFuZXIgY29tcG9uZW50IG1vZHVsZSBvdXRwdXQgYW5kIHdpbGxcbi8vIGJlIGluY2x1ZGVkIGluIHRoZSBmaW5hbCB3ZWJwYWNrIHVzZXIgYnVuZGxlXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgcmF3U2NyaXB0RXhwb3J0cyxcbiAgY29tcGlsZWRUZW1wbGF0ZSxcbiAgc2NvcGVJZCxcbiAgY3NzTW9kdWxlc1xuKSB7XG4gIHZhciBlc01vZHVsZVxuICB2YXIgc2NyaXB0RXhwb3J0cyA9IHJhd1NjcmlwdEV4cG9ydHMgPSByYXdTY3JpcHRFeHBvcnRzIHx8IHt9XG5cbiAgLy8gRVM2IG1vZHVsZXMgaW50ZXJvcFxuICB2YXIgdHlwZSA9IHR5cGVvZiByYXdTY3JpcHRFeHBvcnRzLmRlZmF1bHRcbiAgaWYgKHR5cGUgPT09ICdvYmplY3QnIHx8IHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBlc01vZHVsZSA9IHJhd1NjcmlwdEV4cG9ydHNcbiAgICBzY3JpcHRFeHBvcnRzID0gcmF3U2NyaXB0RXhwb3J0cy5kZWZhdWx0XG4gIH1cblxuICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wXG4gIHZhciBvcHRpb25zID0gdHlwZW9mIHNjcmlwdEV4cG9ydHMgPT09ICdmdW5jdGlvbidcbiAgICA/IHNjcmlwdEV4cG9ydHMub3B0aW9uc1xuICAgIDogc2NyaXB0RXhwb3J0c1xuXG4gIC8vIHJlbmRlciBmdW5jdGlvbnNcbiAgaWYgKGNvbXBpbGVkVGVtcGxhdGUpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IGNvbXBpbGVkVGVtcGxhdGUucmVuZGVyXG4gICAgb3B0aW9ucy5zdGF0aWNSZW5kZXJGbnMgPSBjb21waWxlZFRlbXBsYXRlLnN0YXRpY1JlbmRlckZuc1xuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gc2NvcGVJZFxuICB9XG5cbiAgLy8gaW5qZWN0IGNzc01vZHVsZXNcbiAgaWYgKGNzc01vZHVsZXMpIHtcbiAgICB2YXIgY29tcHV0ZWQgPSBPYmplY3QuY3JlYXRlKG9wdGlvbnMuY29tcHV0ZWQgfHwgbnVsbClcbiAgICBPYmplY3Qua2V5cyhjc3NNb2R1bGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHZhciBtb2R1bGUgPSBjc3NNb2R1bGVzW2tleV1cbiAgICAgIGNvbXB1dGVkW2tleV0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBtb2R1bGUgfVxuICAgIH0pXG4gICAgb3B0aW9ucy5jb21wdXRlZCA9IGNvbXB1dGVkXG4gIH1cblxuICByZXR1cm4ge1xuICAgIGVzTW9kdWxlOiBlc01vZHVsZSxcbiAgICBleHBvcnRzOiBzY3JpcHRFeHBvcnRzLFxuICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyLmpzXG4vLyBtb2R1bGUgaWQgPSAxXG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsImNvbnN0IHJvdXRlcyA9IFtcbiAgICB7IHBhdGg6ICcvYWJvdXQnLCBjb21wb25lbnQ6IHJlcXVpcmUoJy4vcGFnZXMvQWJvdXQudnVlJykgfSxcbiAgICB7IHBhdGg6ICcvY29udGFjdC11cycsIGNvbXBvbmVudDogcmVxdWlyZSgnLi9wYWdlcy9Db250YWN0VXMudnVlJykgIH0sXG4gICAgeyBwYXRoOiAnL3NlcnZpY2VzJywgY29tcG9uZW50OiByZXF1aXJlKCcuL3BhZ2VzL1NlcnZpY2VzLnZ1ZScpICB9LFxuICAgIHsgcGF0aDogJy90cmFja2luZy1udW1iZXInLCBjb21wb25lbnQ6IHJlcXVpcmUoJy4vcGFnZXMvVHJhY2tOdW1iZXIudnVlJykgIH0sXG4gICAgeyBwYXRoOiAnL21vYmlsZS1udW1iZXInLCBjb21wb25lbnQ6IHJlcXVpcmUoJy4vcGFnZXMvQ29udGFjdE51bWJlci52dWUnKSAgfSxcbl1cblxuY29uc3Qgcm91dGVyID0gbmV3IFZ1ZVJvdXRlcih7XG4gICAgcm91dGVzIC8vIHNob3J0IGZvciBgcm91dGVzOiByb3V0ZXNgXG59KVxuXG5yb3V0ZXIucmVwbGFjZSh7IHBhdGg6ICcvdHJhY2tpbmctbnVtYmVyJywgcmVkaXJlY3Q6ICcvJyB9KVxuLy8gbG9hZCBjb21wb25lbnRzXG5WdWUuY29tcG9uZW50KCdtb2RhbCcsIHJlcXVpcmUoJy4vY29tcG9uZW50cy9Nb2RhbC52dWUnKSk7XG5cbnZhciBhcHAgPSBuZXcgVnVlKHtcbiAgICByb3V0ZXIsXG4gICAgZWw6ICcuYXBwJyxcbiAgICBkYXRhOntcbiAgICAgICAgYXBwVGl0bGU6ICdXWUMgR3JvdXAnXG4gICAgfSxcblxuICAgIG1ldGhvZHM6e1xuICAgICAgICBjbG9zZU1lbnUodGl0bGUpIHtcbiAgICAgICAgICAgIHRoaXMuYXBwVGl0bGUgPSB0aXRsZTtcbiAgICAgICAgICAgIGhpZGVEcmF3ZXIoKTtcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBjcmVhdGVkKCl7XG4gICAgICAgIFxuICAgIH1cbn0pXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcmVzb3VyY2VzL2pzL2luZGV4LmpzIiwiPHRlbXBsYXRlPlxuPGRpdiBjbGFzcz1cIm1vZGFsIFwiIDppZD1cImlkXCIgdGFiaW5kZXg9XCItMVwiIHJvbGU9XCJkaWFsb2dcIiBhcmlhLWxhYmVsbGVkYnk9XCJ3eWMtbW9kYWwtbGFiZWxcIj5cbiAgICA8ZGl2IGNsYXNzPVwibW9kYWwtZGlhbG9nXCIgOmNsYXNzPVwibW9kYWxTaXplXCI+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1jb250ZW50IGFuaW1hdGVkIGZhZGVJblwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWhlYWRlclwiPlxuICAgICAgICAgICAgICAgIDxoNCBjbGFzcz1cIm1vZGFsLXRpdGxlXCIgaWQ9XCJ3eWMtbW9kYWwtbGFiZWxcIj5cbiAgICAgICAgICAgICAgICAgICAgPHNsb3QgbmFtZT1cIm1vZGFsLXRpdGxlXCI+TW9kYWwgVGl0bGU8L3Nsb3Q+XG4gICAgICAgICAgICAgICAgPC9oND5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWJvZHlcIj5cbiAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtYm9keVwiPk1vZGFsIEJvZHk8L3Nsb3Q+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1mb290ZXJcIj5cbiAgICAgICAgICAgICAgICA8c2xvdCBuYW1lPVwibW9kYWwtZm9vdGVyXCI+XG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5XCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIj5DbG9zZTwvYnV0dG9uPlxuICAgICAgICAgICAgICAgIDwvc2xvdD5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbjwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgICBwcm9wczoge1xuICAgICAgICBpZDogeyBkZWZhdWx0OiAnd3ljLW1vZGFsJyB9LFxuICAgICAgICBzaXplOiB7IGRlZmF1bHQ6ICdtZCcgfVxuICAgIH0sXG5cbiAgICBjb21wdXRlZDoge1xuICAgICAgICBtb2RhbFNpemU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiAnbW9kYWwtJyArIHRoaXMuc2l6ZTtcbiAgICAgICAgfVxuICAgIH1cbn1cbjwvc2NyaXB0PlxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIE1vZGFsLnZ1ZT84NTJiMjI1YyIsIjx0ZW1wbGF0ZT5cbjxkaXYgaWQ9J2Fib3V0LXBhZ2UnPlxuXHQ8dGFibGUgY2xhc3M9J3RhYmxlJz5cblx0ICAgIDx0ciBpZD0ndHJhbnNwYXJlbmN5Jz5cblx0ICAgICAgICA8dGQ+XG5cdCAgICAgICAgICAgIDxoMz5UcmFuc3BhcmVuY3k8L2gzPlxuXHQgICAgICAgICAgICA8cD5XZSB1bmRlcnRha2UgdG8gYmUgdHJhbnNwYXJlbnQgaW4gYWxsIG91ciBwcm9mZXNzaW9uYWwgY29udGFjdHMuIEluIHByYWN0aWNlLCB0aGlzIG1lYW5zIHdlIHdpbGwgaWRlbnRpZnkgb3VyIGNsaWVudCBpbiBwcm9hY3RpdmUgZXh0ZXJuYWwgcHJvZmVzc2lvbmFsXG5cdCAgICAgICAgICAgIGNvbnRhY3RzIGFuZCBzZXJ2aWNlcyBvZmZlcmVkIGZvciBhIGNsaWVudC48L3A+XG5cblx0ICAgICAgICAgICAgPHA+U29tZXRpbWVzIHdlIGVzdGFibGlzaCBjb2FsaXRpb25zIG9uIGJlaGFsZiBvZiBjbGllbnRzIHRvIHdvcmsgb24gc3BlY2lmaWMgaXNzdWVzLiBXaGVuZXZlciB3ZSBkbyBzbywgd2UgdW5kZXJ0YWtlIHRvIGJlIHRyYW5zcGFyZW50IGFib3V0IFxuXHQgICAgICAgICAgICB0aGUgcHVycG9zZSBvZiB0aGUgY29hbGl0aW9uIGFuZCBwcmluY2lwYWwgZnVuZGVycy48L3A+XG5cdCAgICAgICAgPC90ZD5cblx0ICAgIDwvdHI+XG5cdCAgICA8dHIgaWQ9J2hvbmVzdHknPlxuXHQgICAgICAgIDx0ZD5cblx0ICAgICAgICAgICAgPGgzPkhvbmVzdHk8L2gzPlxuXHQgICAgICAgICAgICA8cD5XZSB1bmRlcnRha2Ugbm90IHRvIGRpc2Nsb3NlIGFueSBjb25maWRlbnRpYWwgaW5mb3JtYXRpb24gcHJvdmlkZWQgdG8gdXMgaW4gdGhlIGNvdXJzZSBvZiBvdXIgd29yayBmb3IgY2xpZW50cywgdW5sZXNzIGV4cGxpY2l0eSBhdXRob3JpemVkIHRvIGRvXG5cdCAgICAgICAgICAgIHNvIG9yIHJlcXVpcmVkIGJ5IGdvdmVybm1lbnRzIG9yIG90aGVyIGxlZ2FsIGF1dGhvcml0aWVzLiBBbGwgV1lDIGVtcGxveWVlcyBhcmUgY29udHJhY3R1YWxseSBvYmxpZ3VlZCB0byByZXNwZWN0IHRoaXMgdW5kZXJ0YWtpbmcgb25cblx0ICAgICAgICAgICAgYWxsIGludGVybmFsIG1hdHRlcnMuPC9wPlxuXG5cdCAgICAgICAgICAgIDxwPldlIHN0b3JlIGFuZCBoYW5kbGUgY29uZmlkZW50aWFsIGluZm9ybWF0aW9uIHNvIGFzIHRvIGVuc3VyZSB0aGF0IG9ubHkgdGhvc2Ugd2hvIGFyZSB3b3JraW5nIHdpdGggdGhlIGluZm9ybWF0aW9uIGhhdmUgYWNjZXNzIHRvIGl0LjwvcD5cblxuXHQgICAgICAgICAgICA8cD5XZSBhZHZpY2Ugb3VyIGNsaWVudHMgdG8gdXBob2xkIGFsbCB0aGVzZSB2YWx1ZXMgaW4gdGhlaXIgb3duIHJlbGF0ZWQgdG8gdGhlIGFkdmljZSB3ZSBwcm92aWRlIHRoZW0uPC9wPlxuXHQgICAgICAgIDwvdGQ+XG5cdCAgICA8L3RyPlxuXHQgICAgPHRyIGlkPSdjb25maWRlbnRpYWxpdHknPlxuXHQgICAgICAgIDx0ZD5cblx0ICAgICAgICAgICAgPGgzPkNvbmZpZGVudGlhbGl0eTwvaDM+XG5cdCAgICAgICAgICAgIDxwPldlIHVuZGVyc3RhbmQgdG8gYmUgaG9uZXN0IGluIGFsbCBvdXIgcHJvZmVzc2lvbmFsIGRlYWxpbmdzLiBXZSB1bmRlcnRha2UgbmV2ZXIga25vd2luZ2x5IHRvIHNwcmVhZCBmYWxzZSBvciBtaXNsZWFkaW5nIGluZm9ybWF0aW9uIGFuZCB0byB0YWtlIFxuXHQgICAgICAgICAgICByZWFzb25hYmxlIGNhcmUgdG8gYXZvaWQgZG9pbmcgc28gaW5hZHZlcnRlbnRseS48L3A+XG5cblx0ICAgICAgICAgICAgPHA+V2UgYWxzbyBhc2sgb3VyIGNsaWVudHMgdG8gYmUgaG9uZXN0IHdpdGggdXMgYW5kIG5vdCB0byByZXF1ZXN0IHRoYXQgd2UgY29tcHJvbWlzZWQgb3VyIHByaW5jaXBsZXMgb3IgdGhlIGxhdy48L3A+XG5cblx0ICAgICAgICAgICAgPHA+V2UgdGFrZSBldmVyeSBwb3NzaWJsZSBzdGVwIHRvIGF2b2lkIGNvbmZsaWN0cyBvZiBpbnRlcmVzdC48L3A+XG5cdCAgICAgICAgPC90ZD5cblx0ICAgIDwvdHI+XG5cdCAgICA8dHIgaWQ9J2ludGVncml0eSc+XG5cdCAgICAgICAgPHRkPlxuXHQgICAgICAgICAgICA8aDM+SW50ZWdyaXR5PC9oMz5cblx0ICAgICAgICAgICAgPHA+V2Ugd2lsbCBvbmx5IHdvcmsgZm9yIHRoZSBvcmdhbml6YXRpb25zIHRoYXQgYXJlIHByZXBhcmVkIHRvIHVwaG9sZCB0aGUgdmFsdWVzIG9mIHRyYW5zcGFyZW5jeSBhbmQgaG9uZXN0eSBpbiB0aGUgY291cnNlIG9mIG91ciB3b3JrIGZvciB0aGVtLjwvcD5cblxuXHQgICAgICAgICAgICA8cD5Tb21ldGltZXMgb3VyIGNsaWVudHMgYXJlIGludm9sdmVkIHdpdGggY29udGVudGl1b3MgaXNzdWVzLiBJdCBpcyBwYXJ0IG9mIG91ciBqb2IgdG8gcHJvdmlkZSBjb21tdW5pY2F0aW9ucyBhc3Npc3RhbmNlIHRvIHRob3NlIGNsaWVudHMuIFxuXHQgICAgICAgICAgICBXZSB1bmRlcnRha2UgdG8gcmVwcmVzZW50IHRob3NlIGNsaWVudHMgb25seSBpbiB3YXlzIGNvbnNpc3RlbnQgd2l0aCB0aGUgdmFsdWVzIG9mIGhvbmVzdHkgYW5kIHRyYW5zcGFyZW5jeSBib3RoIGluIHdoYXQgdGhleSBkbyBhbmQgXG5cdCAgICAgICAgICAgIGluIHdoYXQgd2UgZG8gZm9yIHRoZW0uPC9wPlxuXHQgICAgICAgIDwvdGQ+XG5cdCAgICA8L3RyPlxuXHQ8L3RhYmxlPlxuPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0IHR5cGU9XCJ0ZXh0L2phdmFzY3JpcHRcIj5cblx0ZXhwb3J0IGRlZmF1bHQge31cbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBBYm91dC52dWU/NTVmMTEyNDIiLCI8dGVtcGxhdGU+XG5cdDxkaXYgY2xhc3M9XCJjb2wteHMtMTJcIj5cbiAgICAgICAgPGgzPjxiPiBBZGRyZXNzIDwvYj48YnI+PHNtYWxsPiBVMTEwIEJhbGFndGFzIFN0LCBCYWxhZ3RhcyBWaWxsYXMgQmFyYW5nYXkgMTUgU2FuIElzaWRybyBQYXNheSBDaXR5IDwvc21hbGw+PC9oMz5cbiAgICAgICAgPGgzPjxiPiBDb250YWN0IDwvYj48YnI+PHNtYWxsPiAoMDIpIDM1NCA4MCAyMSA8L3NtYWxsPjwvaDM+XG4gICAgICAgIDxoMz48Yj4gT3BlcmF0aW5nIGhvdXJzPC9iPiA8YnI+PHNtYWxsPiBNb25kYXkgLSBGcmlkYXkgKDhhbSAtIDVwbSk8L3NtYWxsPjwvaDM+XG4gICAgPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0IHR5cGU9XCJ0ZXh0L2phdmFzY3JpcHRcIj5cblx0ZXhwb3J0IGRlZmF1bHQge31cblxuLy8gXHQkKCcjYnRuR29Ub01vYmlsZVBhZ2UnKS5jbGljayhmdW5jdGlvbigpe1xuLy8gICAgIGlmKHN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9tb2JpbGVfbnVtYmVyJykubGVuZ3RoID4gMClcbi8vICAgICB7XG4vLyAgICAgICAgICQoJyNtb2JpbGVfbnVtYmVyJykudmFsKCQoJyN1c2VyX21vYmlsZV9udW1iZXInKS5odG1sKCkpO1xuLy8gICAgICAgICAkKCcjYnRuTW9iaWxlU3VibWl0JykuY2xpY2soKTtcbi8vICAgICB9XG4vLyB9KVxuXG4vLyAkKCcjYnRuTW9iaWxlU3VibWl0JykuY2xpY2soZnVuY3Rpb24oKXtcbiAgICBcbi8vIFx0Y29udGFjdF9udW1iZXIgPSAkKCcjbW9iaWxlX251bWJlcicpLnZhbCgpO1xuLy8gXHRncm91cCA9ICQoJyNwYWNrYWdlLXBhbmVsLWdyb3VwJyk7XG5cbiAgIFxuLy8gICAgIC8vIGFsZXJ0KHN0b3JhZ2UuZ2V0SXRlbSgndXNlcl9tb2JpbGVfbnVtYmVyJykpO1xuLy8gICAgIC8vIDkxNzg0Mjk2Nzlcbi8vICAgICAvLyBzZW9uZy1ob1xuLy8gICAgIC8vIHdvblxuLy8gICAgIC8vIG1hbGVcbi8vIFx0aWYoY29udGFjdF9udW1iZXIubGVuZ3RoID4gNikgLy93cm90ZSAxMCBjaGFyYWN0ZXJzIG9yIHRlbCBudW1cbi8vIFx0e1xuLy8gXHRcdC8vIGNvbnRhY3RfbnVtYmVyID0gJys2MzkxNTU3MDUwMTAnO1xuLy8gXHRcdC8vbGFzdCAxMCBjaGFyYWN0ZXJzXG4vLyBcdFx0Y29udGFjdF9udW1iZXIgPSBjb250YWN0X251bWJlci5zdWJzdHIoY29udGFjdF9udW1iZXIubGVuZ3RoIC0gMTApO1xuLy8gICAgICAgICAvLyA5MTU1NzA1MDEwXG4vLyBcdFx0JC5hamF4KHtcbi8vICAgICAgICAgXHR1cmw6IHdpbmRvdy5iYXNlX3VybCsnYXBpL2dldC1wYWNrYWdlcy1udW1iZXInLFxuLy8gICAgICAgICBcdGRhdGE6e251bWJlcjpjb250YWN0X251bWJlcn0sXG4vLyAgICAgICAgIFx0ZGF0YVR5cGU6J2pzb24nLFxuLy8gICAgICAgICBcdHR5cGU6J1BPU1QnLFxuLy8gICAgICAgICAgICAgYmVmb3JlU2VuZDpmdW5jdGlvbigpe1xuLy8gICAgICAgICAgICAgICAgIHdpbmRvdy5kYXRhX3BhY2thZ2VzID0gbnVsbDtcbi8vICAgICAgICAgICAgICAgICB3aW5kb3cuY2xpZW50cyA9IFtdO1xuLy8gICAgICAgICAgICAgfSxcbi8vICAgICAgICAgXHRzdWNjZXNzOmZ1bmN0aW9uKGRhdGEpe1xuLy8gICAgICAgICAgICAgICAgIHN0b3JhZ2Uuc2V0SXRlbSgndXNlcl9tb2JpbGVfbnVtYmVyJywgY29udGFjdF9udW1iZXIpO1xuXG4vLyAgICAgICAgICAgICAgICAgd2luZG93LnNldE5hbWVBbmROdW1iZXIoKTtcbi8vICAgICAgICAgICAgICAgICBpZihkYXRhWzBdID09IG51bGwpXG4vLyAgICAgICAgIFx0XHR7XG4vLyAgICAgICAgICAgICAgICAgICAgICQuc25hY2tiYXIoe2NvbnRlbnQ6ICdObyBleGlzdGluZyBudW1iZXInfSk7XG4vLyAgICAgICAgIFx0XHR9ZWxzZXtcbi8vICAgICAgICAgICAgICAgICAgICAgaWYoZGF0YS5sZW5ndGggPiAxKVxuLy8gICAgICAgICAgICAgICAgICAgICB7XG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAvL21hbnkgYWNjb3VudHNcbi8vICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5kYXRhX3BhY2thZ2VzID0gZGF0YTtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgIGZvcihqPTA7IGo8ZGF0YS5sZW5ndGg7IGorKylcbi8vICAgICAgICAgICAgICAgICAgICAgICAgIHtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cuY2xpZW50cy5wdXNoKGRhdGFbal0uY2xpZW50KTtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgIH1cbi8vICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNidG5Nb2JpbGVTdWJtaXQnKS5hdHRyKCdocmVmJywnI2Fza2luZ19jcmVkZW50aWFsc19jb250YWluZXInKTtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNidG5Nb2JpbGVTdWJtaXQnKS50YWIoJ3Nob3cnKTtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbi8vICAgICAgICAgICAgICAgICAgICAgfWVsc2Vcbi8vICAgICAgICAgICAgICAgICAgICAge1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgLy8gZ2V0IGRldGFpbHNcbi8vICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5wcm9jZXNzaW5nX2NsaWVudCA9IGRhdGFbMF0uY2xpZW50X2lkO1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LnByb2Nlc3NpbmdfY2xpZW50X25hbWUgPSBkYXRhWzBdLmNsaWVudC5jbGllbnRfbG5hbWUrJywgJytkYXRhWzBdLmNsaWVudC5jbGllbnRfZm5hbWU7XG4gICAgICAgICAgICAgICAgICAgICAgICBcbi8vICAgICAgICAgICAgICAgICAgICAgICAgIHN0b3JhZ2Uuc2V0SXRlbSgndXNlcl9mdWxsX25hbWUnLCB3aW5kb3cucHJvY2Vzc2luZ19jbGllbnRfbmFtZSk7XG4vLyAgICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cuc2V0TmFtZUFuZE51bWJlcigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAkLmFqYXgoe1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybDp3aW5kb3cuYmFzZV91cmwrJ2FwaS9nZXQtcGFja2FnZXMtY2xpZW50Jyxcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhOntjbGllbnRfaWQ6cHJvY2Vzc2luZ19jbGllbnR9LFxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFUeXBlOidqc29uJyxcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOidQT1NUJyxcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICBiZWZvcmVTZW5kOmZ1bmN0aW9uKCl7XG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5wcm9jZXNzaW5nX2NsaWVudF9sZWFkZXIgPSBmYWxzZTtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3M6ZnVuY3Rpb24oc2RhdGEpe1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb21waWxlUGFuZWxzSW5kaXZpZHVhbChzZGF0YSk7XG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKHNkYXRhLmlzX2xlYWRlcilcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LnByb2Nlc3NpbmdfY2xpZW50X2xlYWRlciA9IHNkYXRhLmlzX2xlYWRlcjtcblxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnI2J0bk1vYmlsZVN1Ym1pdCcpLmF0dHIoJ2hyZWYnLCcjY2hvaWNlc19jb250YWluZXInKTtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNidG5Nb2JpbGVTdWJtaXQnKS50YWIoJ3Nob3cnKTtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfWVsc2Vcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnI2J0bk1vYmlsZVN1Ym1pdCcpLmF0dHIoJ2hyZWYnLCcjcGFja2FnZV9jb250YWluZXInKTtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNidG5Nb2JpbGVTdWJtaXQnKS50YWIoJ3Nob3cnKTtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3I6ZnVuY3Rpb24oZXJyb3Ipe1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBhbGVydCgnTm8gZXhpc3RpbmcgbnVtYmVyJyk7XG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgfSlcblxuLy8gICAgICAgICAgICAgICAgICAgICB9XG4vLyAgICAgICAgIFx0XHR9XG4vLyAgICAgICAgIFx0fSxcbi8vICAgICAgICAgXHRlcnJvcjpmdW5jdGlvbihlcnJvcil7XG4vLyAgICAgICAgICAgICAgICAgLy8gJC5zbmFja2Jhcih7Y29udGVudDogJ05vIGV4aXN0aW5nIG51bWJlcid9KTtcbi8vICAgICAgICAgXHR9XG4vLyBcdFx0fSlcbi8vIFx0fWVsc2Vcbi8vICAgICB7XG4vLyAgICAgICAgICQuc25hY2tiYXIoe2NvbnRlbnQ6ICdNaW5pbXVtIG9mIDcgQ2hhcmFjdGVycy4nfSk7XG4vLyAgICAgfVxuLy8gfSlcblxuXG5cblxuLy8gJCgnI2J0bkRldGFpbHNTdWJtaXQnKS5jbGljayhmdW5jdGlvbigpe1xuLy8gICAgIGNsaWVudF9mbmFtZSA9ICQoJyNmbmFtZScpLnZhbCgpO1xuLy8gICAgIGNsaWVudF9sbmFtZSA9ICQoJyNsbmFtZScpLnZhbCgpO1xuLy8gICAgIGNsaWVudF9nZW5kZXIgPSAkKFwiaW5wdXRbbmFtZT1nZW5kZXJdOmNoZWNrZWRcIikudmFsKCk7XG5cblxuLy8gICAgIGZvcihpPTA7IGk8IHdpbmRvdy5jbGllbnRzLmxlbmd0aDsgaSsrKVxuLy8gICAgIHtcbi8vICAgICAgICAgcGFyc2VkID0gd2luZG93LmNsaWVudHNbaV07XG4vLyAgICAgICAgIC8vIGRlYnVnZ2VyO1xuLy8gICAgICAgICBpZihcbi8vICAgICAgICAgICAgICFwYXJzZWRcbi8vICAgICAgICAgKXtcbi8vICAgICAgICAgICAgIGNvbnRpbnVlO1xuLy8gICAgICAgICB9XG5cbi8vICAgICAgICAgaWYoXG4vLyAgICAgICAgICAgICAoXG4vLyAgICAgICAgICAgICAgICAgKFxuLy8gICAgICAgICAgICAgICAgIGNsaWVudF9mbmFtZS50b0xvd2VyQ2FzZSgpID09ICh3aW5kb3cuY2xpZW50c1tpXS5jbGllbnRfZm5hbWUpLnRvTG93ZXJDYXNlKCkudHJpbSgpXG4vLyAgICAgICAgICAgICAgICAgICAgICYmXG4vLyAgICAgICAgICAgICAgICAgY2xpZW50X2xuYW1lLnRvTG93ZXJDYXNlKCkgPT0gKHdpbmRvdy5jbGllbnRzW2ldLmNsaWVudF9sbmFtZSkudG9Mb3dlckNhc2UoKS50cmltKClcbi8vICAgICAgICAgICAgICAgICApXG4vLyAgICAgICAgICAgICB8fFxuLy8gICAgICAgICAgICAgICAgIChcbi8vICAgICAgICAgICAgICAgICBjbGllbnRfbG5hbWUudG9Mb3dlckNhc2UoKSA9PSAod2luZG93LmNsaWVudHNbaV0uY2xpZW50X2ZuYW1lKS50b0xvd2VyQ2FzZSgpLnRyaW0oKVxuLy8gICAgICAgICAgICAgICAgICAgICAmJlxuLy8gICAgICAgICAgICAgICAgIGNsaWVudF9mbmFtZS50b0xvd2VyQ2FzZSgpID09ICh3aW5kb3cuY2xpZW50c1tpXS5jbGllbnRfbG5hbWUpLnRvTG93ZXJDYXNlKCkudHJpbSgpXG4vLyAgICAgICAgICAgICAgICAgKVxuLy8gICAgICAgICAgICAgKVxuLy8gICAgICAgICAgICAgJiZcbi8vICAgICAgICAgICAgIChjbGllbnRfZ2VuZGVyLnRvTG93ZXJDYXNlKCkgPT0gKHdpbmRvdy5jbGllbnRzW2ldLmNsaWVudF9nZW5kZXIpLnRvTG93ZXJDYXNlKCkudHJpbSgpKVxuLy8gICAgICAgICApe1xuLy8gICAgICAgICAgICAgd2luZG93LnByb2Nlc3NpbmdfY2xpZW50ID0gd2luZG93LmNsaWVudHNbaV0uY2xpZW50X2lkO1xuLy8gICAgICAgICAgICAgd2luZG93LnByb2Nlc3NpbmdfY2xpZW50X25hbWUgPSB3aW5kb3cuY2xpZW50c1tpXS5jbGllbnRfbG5hbWUrJywgJyt3aW5kb3cuY2xpZW50c1tpXS5jbGllbnRfZm5hbWU7XG5cbi8vICAgICAgICAgICAgICQuYWpheCh7XG4vLyAgICAgICAgICAgICAgICAgdXJsOndpbmRvdy5iYXNlX3VybCsnYXBpL2dldC1wYWNrYWdlcy1jbGllbnQnLFxuLy8gICAgICAgICAgICAgICAgIGRhdGE6e2NsaWVudF9pZDp3aW5kb3cucHJvY2Vzc2luZ19jbGllbnR9LFxuLy8gICAgICAgICAgICAgICAgIGRhdGFUeXBlOidqc29uJyxcbi8vICAgICAgICAgICAgICAgICB0eXBlOidQT1NUJyxcbi8vICAgICAgICAgICAgICAgICBzdWNjZXNzOmZ1bmN0aW9uKHNkYXRhKXtcbi8vICAgICAgICAgICAgICAgICAgICAgY29tcGlsZVBhbmVsc0luZGl2aWR1YWwoc2RhdGEpO1xuLy8gICAgICAgICAgICAgICAgICAgICBpZihzZGF0YS5pc19sZWFkZXIpXG4vLyAgICAgICAgICAgICAgICAgICAgIHtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5wcm9jZXNzaW5nX2NsaWVudF9sZWFkZXIgPSBzZGF0YS5pc19sZWFkZXI7XG5cbi8vICAgICAgICAgICAgICAgICAgICAgICAgIGNvbXBpbGVQYW5lbHMod2luZG93LmRhdGFfcGFja2FnZXMpO1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI2J0bk1vYmlsZVN1Ym1pdCcpLmF0dHIoJ2hyZWYnLCcjY2hvaWNlc19jb250YWluZXInKTtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNidG5Nb2JpbGVTdWJtaXQnKS50YWIoJ3Nob3cnKTtcbi8vICAgICAgICAgICAgICAgICAgICAgfWVsc2Vcbi8vICAgICAgICAgICAgICAgICAgICAge1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI2J0bk1vYmlsZVN1Ym1pdCcpLmF0dHIoJ2hyZWYnLCcjcGFja2FnZV9jb250YWluZXInKTtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNidG5Nb2JpbGVTdWJtaXQnKS50YWIoJ3Nob3cnKTtcbi8vICAgICAgICAgICAgICAgICAgICAgfVxuLy8gICAgICAgICAgICAgICAgIH0sXG4vLyAgICAgICAgICAgICAgICAgZXJyb3I6ZnVuY3Rpb24oZXJyb3Ipe1xuLy8gICAgICAgICAgICAgICAgIH1cbi8vICAgICAgICAgICAgIH0pXG4gICAgICAgICAgIFxuLy8gICAgICAgICAgICAgcmV0dXJuO1xuLy8gICAgICAgICB9XG4vLyAgICAgfVxuXG4vLyAgICAgJC5zbmFja2Jhcih7Y29udGVudDogJ05vIHJlY29yZHMgZm91bmQhJ30pO1xuLy8gfSlcbjwvc2NyaXB0PlxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyBDb250YWN0TnVtYmVyLnZ1ZT9kODk4ZDU2ZSIsIjx0ZW1wbGF0ZT5cblx0PGRpdiBjbGFzcz1cImNvbC14cy0xMlwiPlxuICAgICAgICA8aDM+PGI+IEFkZHJlc3MgPC9iPjxicj48c21hbGw+IFUxMTAgQmFsYWd0YXMgU3QsIEJhbGFndGFzIFZpbGxhcyBCYXJhbmdheSAxNSBTYW4gSXNpZHJvIFBhc2F5IENpdHkgPC9zbWFsbD48L2gzPlxuICAgICAgICA8aDM+PGI+IENvbnRhY3QgPC9iPjxicj48c21hbGw+ICgwMikgMzU0IDgwIDIxIDwvc21hbGw+PC9oMz5cbiAgICAgICAgPGgzPjxiPiBPcGVyYXRpbmcgaG91cnM8L2I+IDxicj48c21hbGw+IE1vbmRheSAtIEZyaWRheSAoOGFtIC0gNXBtKTwvc21hbGw+PC9oMz5cbiAgICA8L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQgdHlwZT1cInRleHQvamF2YXNjcmlwdFwiPlxuXHRleHBvcnQgZGVmYXVsdCB7fVxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIENvbnRhY3RVcy52dWU/NWIzNmYwOWEiLCI8dGVtcGxhdGU+XG48ZGl2IGlkPSdzZXJ2aWNlcy1wYWdlcyc+XG4gICAgPGJ1dHRvbiB2LWZvcj0nc2VydmljZSBpbiBzZXJ2aWNlcycgY2xhc3M9J2J0biBidG4tbGlzdCcgQGNsaWNrPSdzaG93RGVzY3JpcHRpb24oc2VydmljZS5pZCknPlxuICAgICAgICB7e3NlcnZpY2UudGl0bGV9fVxuICAgIDwvYnV0dG9uPlxuICAgIDxtb2RhbCBpZD0nc2VydmljZU1vZGFsJz5cbiAgICAgICAgPHRlbXBsYXRlIHNsb3Q9XCJtb2RhbC10aXRsZVwiID5Nb2RhbCBUaXRsZTwvdGVtcGxhdGU+XG4gICAgICAgIDx0ZW1wbGF0ZSBzbG90PVwibW9kYWwtYm9keVwiID5Nb2RhbCBUaXRsZTwvdGVtcGxhdGU+XG4gICAgPC9tb2RhbD5cbjwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdCB0eXBlPVwidGV4dC9qYXZhc2NyaXB0XCI+XG5leHBvcnQgZGVmYXVsdCB7XG4gICAgZGF0YSgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHNlcnZpY2VzOiBcbiAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGlkOjEsXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnQUNSLUlDQVJEJyxcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICdBbiBBQ1ItSUNBUkQgaXMgYSBtaWNyb2NoaXAgYmFzZWQsIGNyZWRpdCBjYXJkLXNpemVkLCBpZGVudGlmaWNhdGlvbiBjYXJkIGlzc3VlZCB0byBhbGwgcmVnaXN0ZXJlZCBhbGllbnMgd2hvc2Ugc3RheSBpbiB0aGUgUGhpbGlwcGluZXMgaGFzIGV4Y2VlZGVkIGZpZnR5LW5pbmUgKDU5KSBkYXlzLidcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWQ6MixcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICdBbGllbiBFbXBsb3ltZW50IFBlcm1pdCAoQUVQKScsXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnVGhlIEFSUCBpcyBhIERlcGFydG1lbnRvZiBKdXN0aWNlIC0gQnVyZWF1IG9mIEltbWlncmF0aW9uIChCSSkgaW5pdGlhdGl2ZSBiYXNlZCBvbiB0aHJlZS1waGFzZSBwcm9qZWN0LiBUaGUgRmlyc3QgUGhhc2Ugb2YgdGhlIFByb2plY3QgaW52b2x2ZXMgdGhlIHNldC11cCBhbmQgaW1wbGVtZW50YXRpb24gb2YgQmlvbWV0cmljIGRhdGEtY2FwdHVyaW5nIGRldmljZXMuIFRoZSBTZWNvbmQgYW5kIFRoaXJkIFBoYXNlIHNoYWxsIGltcGxlbWVudCB0aGUgdXNlIG9mIHNlcnZlcnMgYW5kIGRhdGEgYW5hbHl6aW5nIGRldmljZXMuJ1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBpZDozLFxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0JJLUNlcnRpZmljYXRpb24nLFxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogJ1RoZSBCSS1DZXJ0aWZpY2F0aW9uIGlzIGFuIGluZGl2aWR1YWwgY2VydGlmeWluZyB0aGF0IGhlL3NoZSBpcyBub3QgaW4gYW55IG9mIGRlcm9nYXRvcnkgZGF0YWJhc2UsIGxpc3Qgb3IgcmVjb3JkIG9mIHRoZSBCdXJlYXUuJ1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBpZDo0LFxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0JsYWNrIExpc3QnLFxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogJ1RoZSBCbGFjayBMaXN0IGlzIGEgbGlzdCBvciByZWdpc3RlciBvZiBlbnRpdGllcyBvciBwZW9wbGUsIGZvciBvbmUgcmVhc29uIG9yIGFub3RoZXIsIGFyZSBiZWluZyBkZW5pZWQgYSBwYXJ0aWN1bGFyIHByaXZpbGVnZSwgc2VydmljZSwgbW9iaWxpdHksIGFjY2VzcyBvciByZWNvZ25pdGlvbi4nXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGlkOjUsXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnQnVyZWF1IG9mIEludmVzdG1lbnQgKEJPSSknLFxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogJ0J1cmVhdSBvZiBJbnZlc3RtZW50cyBUaGUgUGhpbGlwcGluZSBCb2FyZCBvZiBJbnZlc3RtZW50cyAoQk9JKSwgYW4gYXR0YWNoZWQgYWdlbmN5IG9mIERlcGFydG1lbnQgb2YgVHJhZGUgYW5kIEluZHVzdHJ5IChEVEkpLCBpcyB0aGUgbGVhZCBnb3Zlcm5tZW50IGFnZW5jeSByZXNwb25zaWJsZSBmb3IgdGhlIHByb21vdGlvbiBvZiBpbnZlc3RtZW50cyBpbiB0aGUgUGhpbGlwcGluZXMuJ1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBpZDo2LFxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0J1cmVhdSBvZiBRdWFyYW50aW5lJyxcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICdCdXJlYXUgb2YgUXVhcmFudGluZSAoQk9RKSBpcyB0aGUgaGVhbHRoIGF1dGhvcml0eSBhbmQgYSBsaW5lIGJ1cmVhdSBvZiB0aGUgRGVwYXJ0bWVudCBvZiBIZWFsdGggKERPSCkuIEJPUSBpcyBtYW5kYXRlZCB0byBlbnN1cmUgc2VjdXJpdHkgYWdhaW5zdCB0aGUgaW50cm9kdWN0aW9uIGFuZCBzcHJlYWQgb2YgaW5mZWN0aW91cyBkaXNlYXNlcywgZW1lcmdpbmcgZGlzZWFzZXMgYW5kIHB1YmxpYyBoZWFsdGggZW1lcmdlbmNpZXMgb2YgaW50ZXJuYXRpb25hbCBjb25jZXJuIChQSEVJQykuJ1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBpZDo3LFxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0NlemEgV29ya2luZyBWaXNhJyxcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICdUaGUgQ2FnYXlhbiBFY29ub21pYyBab25lIEF1dGhvcml0eSAoQ0VaQSkgaXMgYSBnb3Zlcm5tZW50IG93bmVkIGFuZCBjb250cm9sbGVkIGNvcnBvcmF0aW9uIHRoYXQgd2FzIGNyZWF0ZWQgYnkgdmlydHVlIG9mIFJlcHVibGljIEFjdCA3OTIyLCBvdGhlcndpc2Uga25vd24gYXMgdGhlIOKAnENhZ2F5YW4gU3BlY2lhbCBFY29ub21pYyBab25lIEFjdCBvZiAxOTk14oCzLidcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWQ6OCxcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICdDb21wYW55IFJlZ2lzdHJhdGlvbicsXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnVGhlIGNvbXBhbnkgbmFtZSBtdXN0IGJlIG9mZmljaWFsbHkgYXBwcm92ZWQgYnkgdGhlIFJlZ2lzdHJhciBvZiBDb21wYW5pZXMuIE9uIGFwcGx5aW5nIHRvIHRoZSBSZWdpc3RyYXIgZm9yIHRoZSBhcHByb3ZhbCBvZiBhIG5hbWUsIGl0IGlzIHJlY29tbWVuZGVkIHRoYXQgdHdvIG9yIHRocmVlIHBvc3NpYmxlIG5hbWVzIGVuZGluZyB3aXRoIHRoZSB3b3JkIOKAnGxpbWl0ZWTigJ0gYmUgc3VibWl0dGVkIGFzIHRoaXMgbWF5IGF2b2lkIHVubmVjZXNzYXJ5IGRlbGF5cy4nXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGlkOjksXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnRGVwYXJ0bWVudCBvZiBKdXN0aWNlIChET0opJyxcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICdUaGUgRGVwYXJ0bWVudCBvZiBKdXN0aWNlIChET0opIGlzIHRoZSBqdWRpY2lhbCBkZXBhcnRtZW50IG9mIHRoZSBQaGlsaXBwaW5lIGdvdmVybm1lbnQgcmVzcG9uc2libGUgZm9yIHVwaG9sZGluZyB0aGUgcnVsZSBvZiBsYXcgaW4gdGhlIFBoaWxpcHBpbmVzLiBJdCBpcyB0aGUgZ292ZXJubWVudFxcJ3MgcHJpbmNpcGFsIGxhdyBhZ2VuY3ksIHNlcnZpbmcgYXMgaXRzIGxlZ2FsIGNvdW5zZWwgYW5kIHByb3NlY3V0aW9uIGFybS4nXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGlkOjEwLFxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0VtYmFzc3kgU2VydmljZXMnLFxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogJ1RoZSBFbWJhc3N5IFNlcnZpY2VzIGlzIGhlbHAgYW5kIGFkdmljZSBwcm92aWRlZCBieSB0aGUgZGlwbG9tYXRpYyBhZ2VudHMgb2YgYSBjb3VudHJ5IHRvIGNpdGl6ZW5zIG9mIHRoYXQgY291bnRyeSB3aG8gYXJlIGxpdmluZyBvciB0cmF2ZWxpbmcgb3ZlcnNlYXMuJ1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBpZDoxMSxcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICdFbWlncmF0aW9uIENsZWFyYW5jZSBDZXJ0aWZpY2F0ZSAoRUNDKScsXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnVGhlIEVtaWdyYXRpb24gQ2xlYXJhbmNlIENlcnRpZmljYXRlIChFQ0MpLCBhbHRob3VnaCBub3QgdGVjaG5pY2FsbHkgYSB2aXNhIGlzIGdyb3VwZWQgd2l0aCB0aGVtIGJlY2F1c2UgaXQgaXMgb2Z0ZW4gcmVmZXJyZWQgdG8gYnkgZm9yZWlnbiBuYXRpb25hbHMgYXMgYW4gZXhpdCB2aXNhLCBhbmQgZnJhbmtseSBpdCBoYXMgdGhhdCBraW5kIG9mIHBvd2VyLCB3aXRob3V0IGl0IHlvdSBkb27igJl0IGdldCB0byBsZWF2ZSB0aGUgY291bnRyeSB3aGVuIHlvdSBwbGFubmVkLidcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWQ6MTIsXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnTlNPIENlcnRpZmljYXRlcycsXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnVGhlIE5hdGlvbmFsIFN0YXRpc3RpY3MgT2ZmaWNlIChOU08pIGlzIHRoZSBwcmltYXJ5IHN0YXRpc3RpY2FsIGFnZW5jeSBvZiB0aGUgZ292ZXJubWVudCBtYW5kYXRlZCB0byBjb2xsZWN0LCBjb21waWxlLCBjbGFzc2lmeSwgcHJvZHVjZSwgcHVibGlzaCwgYW5kIGRpc3NlbWluYXRlIGdlbmVyYWwtcHVycG9zZSBzdGF0aXN0aWNzIGFzIHByb3ZpZGVkIGZvciBpbiBDb21tb253ZWFsdGggQWN0IE5vLiA1OTEuJ1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBpZDoxMyxcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICdSZS1zdGFtcGluZyBvZiBWaXNhJyxcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICdGb3JlaWduIG5hdGlvbmFscyB3aXRoIHZpc2FzIHRoYXQgYXJlIG5vdCBmdWxseSBpbXBsZW1lbnRlZCBvciBmb3IgcmVhc29ucyBvZiBsb3N0IG9yIGRhbWFnZWQgcGFzc3BvcnRzLidcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWQ6MTQsXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnU3BlY2lhbCBSZXRpcmVtZW50IFJlc2lkZW5jZSBWaXNhIChTUlJWKScsXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnVGhlIEluZm9ybWF0aW9uIEd1aWRlIHRvIHRoZSBTcGVjaWFsIFJlc2lkZW50IFJldGlyZWVcXCdzIFZpc2EgKFNSUlYpIGlzIGRlc2lnbmVkIGFzIGEgZGV0YWlsZWQgcmVmZXJlbmNlIHRvIGFzc2lzdCByZXRpcmVlIGFwcGxpY2FudHMgaW4gdW5kZXJzdGFuZGluZyB0aGUgZGlmZmVyZW50IFNSUlYgT3B0aW9ucywgdGhlIGFwcGxpY2F0aW9uIHJlcXVpcmVtZW50cyBhbmQgcHJvY2VzcywgYW5kIHRoZSBvYmxpZ2F0aW9ucyBvZiBiZWluZyBhIFBSQSByZXRpcmVlIG1lbWJlci4nXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGlkOjE1LFxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ1N0dWRlbnQgVmlzYSAoOUYpJyxcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICdUaGUgYXBwbGljYW50IGZvciBzdHVkZW50IHZpc2EsIGhhdmluZyBtZWFucyBzdWZmaWNpZW50IGZvciBlZHVjYXRpb24gYW5kIHN1cHBvcnQgaW4gdGhlIFBoaWxpcHBpbmVzLCBtdXN0IGJlIGF0IGxlYXN0IGVpZ2h0ZWVuICgxOCkgeWVhcnMgb2xkIGFuZCBzZWVrcyB0byBlbnRlciB0aGUgUGhpbGlwcGluZXMgdGVtcG9yYXJpbHkgYW5kIHNvbGVseSBmb3IgdGhlIHB1cnBvc2Ugb2YgdGFraW5nIHVwIGEgY291cnNlIG9mIHN0dWR5IGhpZ2hlciB0aGFuIGhpZ2ggc2Nob29sIGF0IGEgdW5pdmVyc2l0eSwgc2VtaW5hcnksIGFjYWRlbXksIGNvbGxlZ2Ugb3Igc2Nob29sLCBhcHByb3ZlZCBmb3IgZm9yZWlnbiBzdHVkZW50cyBieSB0aGUgQ29tbWlzc2lvbmVyIG9mIEltbWlncmF0aW9uLidcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWQ6MTYsXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnVGhlIE5hdGlvbmFsIEJ1cmVhdSBvZiBJbnZlc3RpZ2F0aW9uIChOQkkpJyxcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICdUaGUgTmF0aW9uYWwgQnVyZWF1IG9mIEludmVzdGlnYXRpb24gKE5CSSkgaXMgYW4gYWdlbmN5IG9mIHRoZSBQaGlsaXBwaW5lIGdvdmVybm1lbnQgdW5kZXIgdGhlIERlcGFydG1lbnQgb2YgSnVzdGljZSwgcmVzcG9uc2libGUgZm9yIGhhbmRsaW5nIGFuZCBzb2x2aW5nIG1ham9yIGhpZ2gtcHJvZmlsZSBjYXNlcyB0aGF0IGFyZSBpbiB0aGUgaW50ZXJlc3Qgb2YgdGhlIG5hdGlvbi4nXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGlkOjE3LFxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ1RvdXJpc3QgVmlzYScsXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnQSBUb3VyaXN0IFZpc2EgaXMgYSBjb25kaXRpb25hbCBhdXRob3JpemF0aW9uIGdyYW50ZWQgYnkgYSBjb3VudHJ5ICh0eXBpY2FsbHkgdG8gYSBmb3JlaWduZXIpIHRvIGVudGVyIGFuZCB0ZW1wb3JhcmlseSByZW1haW4gd2l0aGluLCBvciB0byBsZWF2ZSB0aGF0IGNvdW50cnkuIFZpc2FzIHR5cGljYWxseSBpbmNsdWRlIGxpbWl0cyBvbiB0aGUgZHVyYXRpb24gb2YgdGhlIGZvcmVpZ25lclxcJ3Mgc3RheSwgdGVycml0b3J5IHdpdGhpbiB0aGUgY291bnRyeSB0aGV5IG1heSBlbnRlciwgdGhlIGRhdGVzIHRoZXkgbWF5IGVudGVyLCBvciB0aGUgbnVtYmVyIG9mIHBlcm1pdHRlZCB2aXNpdHMuJ1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBpZDoxOCxcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICdWaXNhIFN0YXR1cyBWZXJpZmljYXRpb24nLFxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogJ0lmIHlvdSBoYXZlIGFwcGxpZWQgb3IgcGV0aXRpb25lZCBmb3IgYW4gaW1taWdyYXRpb24gYmVuZWZpdCwgeW91IGNhbiBjaGVjayB0aGUgc3RhdHVzIG9mIHlvdXIgY2FzZSBvbmxpbmUuJ1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBpZDoxOSxcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICdXYWl2ZXIgZm9yIEV4Y2x1c2lvbiBHcm91bmQnLFxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogJ1VuZGVyIFNlY3Rpb24gMjkoYSkgKDEyKSBvZiB0aGUgQ29tbW9ud2VhbHRoIEFjdCBOby4gNjEzIG9yIHRoZSBQaGlsaXBwaW5lIEltbWlncmF0aW9uIEFjdCBvZiAxOTQwLCBjaGlsZHJlbiBiZWxvdyBmaWZ0ZWVuICgxNSkgeWVhcnMgb2YgYWdlIHdobyBhcmUgdW5hY2NvbXBhbmllZCBieSBvciBub3QgY29taW5nIHRvIGEgcGFyZW50LCBhcmUgY2xhc3NpZmllZCBhcyBleGNsdWRhYmxlLiBJbiBvcmRlciBmb3IgdGhlbSB0byBiZSBhZG1pdHRlZCBpbnRvIHRoZSBQaGlsaXBwaW5lcywgdGhleSBtdXN0IHNlY3VyZSBhIFdhaXZlciBvZiBFeGNsdXNpb24gR3JvdW5kIChXRUcpIGZyb20gdGhlIEJ1cmVhdSBvZiBJbW1pZ3JhdGlvbiwgdXBvbiBwcm9wZXIgY29vcmRpbmF0aW9uIHdpdGggdGhlIERlcGFydG1lbnQgb2YgRm9yZWlnbiBBZmZhaXJzIChERkEpLidcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaWQ6MjAsXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnV29ya2luZyBWaXNhICg5RyknLFxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogJ0EgZm9yZWlnbmVyIHdobyBkZXNpcmVzIHRvIHdvcmsgaW4gdGhlIFBoaWxpcHBpbmVzIG11c3Qgc2VjdXJlIGEgOWcgb3Igd29yayB2aXNhIGZyb20gdGhlIEJ1cmVhdSBvZiBJbW1pZ3JhdGlvbi4gVGhlIHByb2Nlc3Mgd2lsbCBiZSBhIGNoYW5nZSBvZiBTdGF0dXMgdG8gUHJlLUFycmFuZ2VkIEVtcGxveWVlIHVuZGVyIFNlY3Rpb24gOShHKSBvZiB0aGUgUGhpbGlwcGluZSBJbW1pZ3JhdGlvbiBBY3Qgb2YgMTk0MCwgYXMgYW1lbmRlZC4nXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGlkOjIxLFxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ090aGVyIFNlcnZpY2VzJyxcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICdXZSBhc3Npc3QgT3RoZXIgU2VydmljZXMgbGlrZSBBZmZpZGF2aXQsIExvc3QgSUNhcmQsIGFuZCBUaW4gQ2FyZC4nXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIF1cbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBtZXRob2RzOntcbiAgICAgICAgc2hvd0Rlc2NyaXB0aW9uKGlkKXtcbiAgICAgICAgICAgIHZhciBzZXJ2aWNlID0gdGhpcy5zZXJ2aWNlcy5maWx0ZXIob2JqID0+IHsgcmV0dXJuIG9iai5pZCA9PT0gaWR9KTtcbiAgICAgICAgICAgIGlmKHNlcnZpY2UpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgc2VydmljZSA9IHNlcnZpY2VbMF07XG4gICAgICAgICAgICAgICAgJCgnLm1vZGFsLXRpdGxlJykuaHRtbChzZXJ2aWNlLnRpdGxlKTtcbiAgICAgICAgICAgICAgICAkKCcubW9kYWwtYm9keScpLmh0bWwoc2VydmljZS5kZXNjcmlwdGlvbik7XG4gICAgICAgICAgICAgICAgJCgnI3NlcnZpY2VNb2RhbCcpLm1vZGFsKCd0b2dnbGUnKVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufVxuXG4vLyAkKGRvY3VtZW50KS5yZWFkeSgoKT0+e1xuLy8gICAgICQoJy5tb2RhbC1ib2R5Jykuc2Nyb2xsKCgpPT57XG4vLyAgICAgICAgIGNsZWFyVGltZW91dCgkLmRhdGEodGhpcywgJ3Njcm9sbFRpbWVyJykpO1xuLy8gICAgICAgICAkKCcubW9kYWwtaGVhZGVyJykuY3NzKCdib3gtc2hhZG93JywnMCAycHggMnB4IDAgcmdiYSgwLDAsMCwwLjE0KSwgMCAxcHggNXB4IDAgcmdiYSgwLDAsMCwwLjEyKSwgMCAzcHggMXB4IC0ycHggcmdiYSgwLDAsMCwwLjIpJyk7XG4vLyAgICAgICAgICQoJy5tb2RhbC1mb290ZXInKS5jc3MoJ2JveC1zaGFkb3cnLCcwIDJweCAycHggMCByZ2JhKDAsMCwwLDAuMTQpLCAwIDFweCA1cHggMCByZ2JhKDAsMCwwLDAuMTIpLCAwIDNweCAxcHggLTJweCByZ2JhKDAsMCwwLDAuMiknKTtcbi8vICAgICAgICAgJC5kYXRhKHRoaXMsICdzY3JvbGxUaW1lcicsIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4vLyAgICAgICAgICAgICAvLyBkbyBzb21ldGhpbmdcbi8vICAgICAgICAgICAgICQoJy5tb2RhbC1oZWFkZXInKS5jc3MoJ2JveC1zaGFkb3cnLCdub25lJyk7XG4vLyAgICAgICAgICAgICAkKCcubW9kYWwtZm9vdGVyJykuY3NzKCdib3gtc2hhZG93Jywnbm9uZScpO1xuLy8gICAgICAgICB9LCAyNTApKTtcbi8vICAgICB9KVxuLy8gfSlcblxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFNlcnZpY2VzLnZ1ZT8xOTc3MDliZSIsIjx0ZW1wbGF0ZT5cblx0PGRpdiBjbGFzcz1cImNvbC1tZC0xMlwiPlxuICAgICAgICA8aDM+VHJhY2tpbmcgTnVtYmVyXG4gICAgICAgIFx0PGJyPlxuXHRcdFx0PHNtYWxsPk1vbml0b3IgeW91ciBwYWNrYWdlIGJ5IGVudGVyaW5nIHRoZSB0cmFja2luZyBudW1iZXIgZ2l2ZW4gYnkgV1lDIEJ1c2luZXNzIENvbnN1bHRhbmN5Ljwvc21hbGw+XG4gICAgICAgIDwvaDM+XG4gICAgICAgIDxkaXYgY2xhc3M9J2NhcmQnPlxuXHRcdFx0PGRpdiBjbGFzcz1cImNhcmQtYm9keVwiPlxuXHRcdFx0XHQ8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCBmb3JtLWdyb3VwLWxnICBsYWJlbC1mbG9hdGluZ1wiIDpjbGFzcz1cInsgJ2hhcy1lcnJvcic6IGZvcm0uZXJyb3JzLmhhcygncGFja2FnZScpIH1cIj5cblx0XHRcdFx0XHQ8bGFiZWwgY2xhc3M9XCJjb250cm9sLWxhYmVsXCIgPlRyYWNraW5nIE51bWJlcjwvbGFiZWw+XG5cdFx0XHRcdFx0PGlucHV0IHYtbW9kZWw9J2Zvcm0ucGFja2FnZScgaWQ9J3RyYWNraW5nTnVtYmVyJyBjbGFzcz0nZm9ybS1jb250cm9sJz5cblx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0PHAgY2xhc3M9XCJ0ZXh0LWRhbmdlciBoZWxwLWJsb2NrXCIgdi1pZj1cImZvcm0uZXJyb3JzLmhhcygncGFja2FnZScpXCIgdi10ZXh0PVwiZm9ybS5lcnJvcnMuZ2V0KCdwYWNrYWdlJylcIj48L3A+XG5cdFx0XHRcdDxjZW50ZXI+XG5cdFx0XHRcdFx0PGJ1dHRvbiBAY2xpY2s9J3N1Ym1pdCgpJyBjbGFzcz0ndGV4dC1jZW50ZXIgYnRuIGJ0bi1sZyBidG4tcmFpc2VkIGJ0bi1wcmltYXJ5Jz5TdWJtaXQ8L2J1dHRvbj5cblx0XHRcdFx0PC9jZW50ZXI+XG5cdFx0XHQ8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0IHR5cGU9XCJ0ZXh0L2phdmFzY3JpcHRcIj5cbmV4cG9ydCBkZWZhdWx0IHtcblx0ZGF0YSgpIHtcblx0XHRyZXR1cm4ge1xuXHRcdFx0Zm9ybTpuZXcgRm9ybSh7XG5cdFx0XHRcdHBhY2thZ2U6JycsXG5cdFx0XHR9LCB7IGJhc2VVUkw6IHdpbmRvdy5iYXNlX3VybCB9KSxcblx0XHR9XG5cdH0sXG5cblx0bWV0aG9kczp7XG5cdFx0c3VibWl0KCl7XG5cdFx0XHR0aGlzLmZvcm1cblx0XHRcdFx0LnN1Ym1pdCgncG9zdCcsd2luZG93LnRyYWNraW5nX251bWJlcl91cmwpXG4gICAgICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzdWx0KVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmNhdGNoKCQubm9vcCk7XG5cdFx0fVxuXHR9XG59XG5cbi8vIGVuZFxuXG4vLyAkKCcjYnRuVHJhY2tpbmdTdWJtaXQnKS5jbGljayhmdW5jdGlvbigpe1xuXG4vLyAgICAgdHJhY2tpbmdfbnVtYmVyID0gJCgnI3RyYWNraW5nX251bWJlcicpLnZhbCgpO1xuLy8gICAgICQuYWpheCh7XG4vLyAgICAgICAgIHVybDp3aW5kb3cuYmFzZV91cmwrJ2FwaS90cmFjay8nK3RyYWNraW5nX251bWJlcixcbi8vICAgICAgICAgdHlwZTogJ0dFVCcsXG4vLyAgICAgICAgIGRhdGFUeXBlOiAnanNvbicsXG4vLyAgICAgICAgIGJlZm9yZVNlbmQoKXtcbi8vICAgICAgICAgICAgICQoJyN0cmFja2luZ190YWJsZScpLmh0bWwoJycpO1xuLy8gICAgICAgICB9LFxuLy8gICAgICAgICBzdWNjZXNzOmZ1bmN0aW9uKGRhdGEpXG4vLyAgICAgICAgIHtcbi8vICAgICAgICAgICAgIHRkID0gJyc7XG4vLyAgICAgICAgICAgICBpZihkYXRhKVxuLy8gICAgICAgICAgICAge1xuXG4vLyAgICAgICAgICAgICAgICAgLy9ncm91cCBwYWNrYWdlXG4vLyAgICAgICAgICAgICAgICAgaWYodHJhY2tpbmdfbnVtYmVyLnRvVXBwZXJDYXNlKCkuaW5kZXhPZignR0wnKSA+IC0xKVxuLy8gICAgICAgICAgICAgICAgIHtcblxuLy8gICAgICAgICAgICAgICAgICAgICBwYWNrYWdlX2Nvc3QgPSAwO1xuLy8gICAgICAgICAgICAgICAgICAgICB0ZCArPSBcIjx0YWJsZSBjbGFzcz0ndGFibGUgdGFibGUtYm9yZGVyZWQnPlxcXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICA8dGhlYWQ+XFxcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dHI+XFxcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRoPk5hbWU8L3RoPlxcXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0aD5DbGllbnQgIzwvdGg+XFxcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRoPlRvdGFsIFNlcnZpY2UgQ29zdDwvdGg+XFxcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RyPlxcXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICA8L3RoZWFkPlxcXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICA8dGJvZHkgaWQ9Jyc+XCI7XG4gICAgICAgICAgICAgICAgICAgICAgICBcbi8vICAgICAgICAgICAgICAgICAgICAgZm9yKHZhciBpPTA7IGk8IGRhdGEuc2VydmljZXMubGVuZ3RoOyBpKyspXG4vLyAgICAgICAgICAgICAgICAgICAgIHtcbi8vICAgICAgICAgICAgICAgICAgICAgICAgIHRkKz0gJzx0ciBpZD1cInNlZURldGFpbHNcIiBkYXRhLWdyb3VwPVwiJytkYXRhLnNlcnZpY2VzW2ldLmdyb3VwX2lkKydcIiBkYXRhLWNsaWVudD1cIicrZGF0YS5zZXJ2aWNlc1tpXS5jbGllbnRfaWQrJ1wiICA+Jztcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZCs9ICc8dGQ+Jyt0b1RpdGxlQ2FzZShkYXRhLnNlcnZpY2VzW2ldLmNsaWVudF9sbmFtZSkrJywgJyt0b1RpdGxlQ2FzZShkYXRhLnNlcnZpY2VzW2ldLmNsaWVudF9mbmFtZSkrJzwvdGQ+Jztcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZCs9ICc8dGQ+JytkYXRhLnNlcnZpY2VzW2ldLmNsaWVudF9pZCsnPC90ZD4nO1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRkKz0gJzx0ZD4nK251bWJlcldpdGhDb21tYXMoZGF0YS5zZXJ2aWNlc1tpXS50b3RhbF9jb3N0KSsnPC90ZD4nO1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgdGQrPSAnPC90cj4nO1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgdG90YWxfY29zdCA9ICcwJytkYXRhLnNlcnZpY2VzW2ldLnRvdGFsX2Nvc3Q7XG4vLyAgICAgICAgICAgICAgICAgICAgIH1cblxuLy8gICAgICAgICAgICAgICAgICAgICB0ZCs9ICAgIFwiPC90Ym9keT5cXFxuLy8gICAgICAgICAgICAgICAgICAgICA8L3RhYmxlPlwiO1xuXG4vLyAgICAgICAgICAgICAgICAgICAgICQoJyN0cmFja2luZ190YWJsZScpLmh0bWwodGQpO1xuLy8gICAgICAgICAgICAgICAgICAgICAkKCcjY2xpZW50X25hbWUnKS5odG1sKHRyYWNraW5nX251bWJlcik7XG4vLyAgICAgICAgICAgICAgICAgICAgICQoJyNjbGllbnRfaWQnKS5odG1sKGRhdGEuZ3JvdXApO1xuICAgICAgICAgICAgICAgICAgICBcbi8vICAgICAgICAgICAgICAgICAgICAgcGFja2FnZV9jb3N0ID0gcGFyc2VGbG9hdChkYXRhLmNvc3QpO1xuXG4vLyAgICAgICAgICAgICAgICAgICAgICQoJyN0b3RhbF9wYWNrYWdlX2Nvc3QnKS5odG1sKG51bWJlcldpdGhDb21tYXMocGFja2FnZV9jb3N0KSk7XG4vLyAgICAgICAgICAgICAgICAgICAgICQoJyNpbml0aWFsX2RlcG9zaXQnKS5odG1sKG51bWJlcldpdGhDb21tYXMoZGF0YS5kZXBvc2l0KSk7XG4vLyAgICAgICAgICAgICAgICAgICAgICQoJyN0b3RhbF9wYXltZW50JykuaHRtbChudW1iZXJXaXRoQ29tbWFzKGRhdGEucGF5bWVudCkpO1xuLy8gICAgICAgICAgICAgICAgICAgICAkKCcjdG90YWxfcmVmdW5kJykuaHRtbChudW1iZXJXaXRoQ29tbWFzKGRhdGEucmVmdW5kKSk7XG4vLyAgICAgICAgICAgICAgICAgICAgICQoJyN0b3RhbF9kaXNjb3VudCcpLmh0bWwobnVtYmVyV2l0aENvbW1hcyhkYXRhLmRpc2NvdW50KSk7XG4vLyAgICAgICAgICAgICAgICAgICAgICQoJyNhdmFpbGFibGVfYmFsYW5jZScpLmh0bWwobnVtYmVyV2l0aENvbW1hcyhkYXRhLmJhbGFuY2UpKTtcblxuLy8gICAgICAgICAgICAgICAgIH1lbHNlIC8vbWVtYmVyIHBhY2thZ2Ugb3Igbm9ybWFsIHBhY2thZ2Vcbi8vICAgICAgICAgICAgICAgICB7XG5cbi8vICAgICAgICAgICAgICAgICAgICAgcGFja2FnZV9jb3N0ID0gMDtcbi8vICAgICAgICAgICAgICAgICAgICAgdGQgKz0gXCI8dGFibGUgY2xhc3M9J3RhYmxlIHRhYmxlLWJvcmRlcmVkJz5cXFxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGhlYWQ+XFxcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0cj5cXFxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0aD5TZXJ2aWNlIFN0YXR1czwvdGg+XFxcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGg+RGF0ZSBDcmVhdGVkPC90aD5cXFxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0aD5TZXJ2aWNlIERldGFpbDwvdGg+XFxcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGg+U2VydmljZSBDb3N0PC90aD5cXFxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0aD5BZGRpdGlvbmFsIENoYXJnZTwvdGg+XFxcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGg+U2VydmljZSBGZWU8L3RoPlxcXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRoPlRvdGFsIENvc3Q8L3RoPlxcXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RyPlxcXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdGhlYWQ+XFxcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRib2R5ID5cIjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxuLy8gICAgICAgICAgICAgICAgICAgICBmb3IodmFyIGk9MDsgaTwgZGF0YS5saXN0Lmxlbmd0aDsgaSsrKVxuLy8gICAgICAgICAgICAgICAgICAgICB7XG4vLyAgICAgICAgICAgICAgICAgICAgICAgICB0ZCs9ICc8dHIgaWQ9XCJzZWVMb2dzXCIgZGF0YS1pZD1cIicrZGF0YS5saXN0W2ldLmlkKydcIj4nO1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRkKz0gJzx0ZD4nK2RhdGEubGlzdFtpXS5zdGF0dXMrJzwvdGQ+Jztcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZCs9ICc8dGQ+JytkYXRhLmxpc3RbaV0uc2VydmljZV9kYXRlKyc8L3RkPic7XG4vLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGQrPSAnPHRkPicrZGF0YS5saXN0W2ldLmRldGFpbCsnPC90ZD4nO1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRkKz0gJzx0ZD4nK251bWJlcldpdGhDb21tYXMoZGF0YS5saXN0W2ldLmFtb3VudCkrJzwvdGQ+Jztcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZCs9ICc8dGQ+JytudW1iZXJXaXRoQ29tbWFzKGRhdGEubGlzdFtpXS5jaGFyZ2UyKSsnPC90ZD4nO1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRkKz0gJzx0ZD4nK251bWJlcldpdGhDb21tYXMoZGF0YS5saXN0W2ldLmNoYXJnZSkrJzwvdGQ+Jztcbi8vICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZCs9ICc8dGQ+JysgbnVtYmVyV2l0aENvbW1hcyhwYXJzZUZsb2F0KGRhdGEubGlzdFtpXS5jaGFyZ2UpK3BhcnNlRmxvYXQoZGF0YS5saXN0W2ldLmFtb3VudCkpICsnPC90ZD4nO1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgdGQrPSAnPC90cj4nO1xuLy8gICAgICAgICAgICAgICAgICAgICAgICAgcGFja2FnZV9jb3N0ICs9IChwYXJzZUZsb2F0KGRhdGEubGlzdFtpXS5jaGFyZ2UpK3BhcnNlRmxvYXQoZGF0YS5saXN0W2ldLmFtb3VudCkpO1xuLy8gICAgICAgICAgICAgICAgICAgICB9XG4vLyAgICAgICAgICAgICAgICAgICAgICB0ZCs9ICAgIFwiPC90Ym9keT5cXFxuLy8gICAgICAgICAgICAgICAgICAgICA8L3RhYmxlPlwiO1xuLy8gICAgICAgICAgICAgICAgICAgICAkKCcjdHJhY2tpbmdfdGFibGUnKS5odG1sKHRkKTtcbi8vICAgICAgICAgICAgICAgICAgICAgJCgnI2NsaWVudF9uYW1lJykuaHRtbChkYXRhLmNsaWVudC5jbGllbnRfbG5hbWUrJywgJytkYXRhLmNsaWVudC5jbGllbnRfZm5hbWUpO1xuLy8gICAgICAgICAgICAgICAgICAgICAkKCcjY2xpZW50X2lkJykuaHRtbChkYXRhLmNsaWVudC5jbGllbnRfaWQpO1xuXG4vLyAgICAgICAgICAgICAgICAgICAgIHBhY2thZ2VfY29zdCA9IGRhdGEuY29zdDtcbi8vICAgICAgICAgICAgICAgICAgICAgJCgnI3RvdGFsX3BhY2thZ2VfY29zdCcpLmh0bWwobnVtYmVyV2l0aENvbW1hcyhwYWNrYWdlX2Nvc3QpKTtcbi8vICAgICAgICAgICAgICAgICAgICAgJCgnI2luaXRpYWxfZGVwb3NpdCcpLmh0bWwobnVtYmVyV2l0aENvbW1hcyhkYXRhLmRlcG9zaXQpKTtcbi8vICAgICAgICAgICAgICAgICAgICAgJCgnI3RvdGFsX3BheW1lbnQnKS5odG1sKG51bWJlcldpdGhDb21tYXMoZGF0YS5wYXltZW50KSk7XG4vLyAgICAgICAgICAgICAgICAgICAgICQoJyN0b3RhbF9yZWZ1bmQnKS5odG1sKG51bWJlcldpdGhDb21tYXMoZGF0YS5yZWZ1bmQpKTtcbi8vICAgICAgICAgICAgICAgICAgICAgJCgnI3RvdGFsX2Rpc2NvdW50JykuaHRtbChudW1iZXJXaXRoQ29tbWFzKGRhdGEuZGlzY291bnQpKTtcbi8vICAgICAgICAgICAgICAgICAgICAgJCgnI2F2YWlsYWJsZV9iYWxhbmNlJykuaHRtbChudW1iZXJXaXRoQ29tbWFzKGRhdGEuYmFsYW5jZSkpO1xuLy8gICAgICAgICAgICAgICAgIH1cblxuLy8gICAgICAgICAgICAgICAgICQoJyNidG5UcmFja2luZ1N1Ym1pdCcpLnRhYignc2hvdycpO1xuLy8gICAgICAgICAgICAgfVxuXG4vLyAgICAgICAgIH0sXG4vLyAgICAgICAgIGVycm9yOmZ1bmN0aW9uKGRhdGEpXG4vLyAgICAgICAgIHtcbi8vICAgICAgICAgfVxuLy8gICAgICAgICAvLyBodHRwOi8vdG9wd3ljLmNvbS9hcGkvdHJhY2svNDg5NzQ5NlxuLy8gICAgIH0pXG4vLyB9KVxuXG4vLyAkKCdib2R5Jykub24oJ2NsaWNrJywnW2lkPVwic2VlRGV0YWlsc1wiXScsZnVuY3Rpb24oKXtcbi8vICAgICBwb3N0X2RhdGEgPSB7fTtcbi8vICAgICAgcG9zdF9kYXRhLmdyb3VwX2lkID0gJCh0aGlzKS5kYXRhKCdncm91cCcpXG4vLyAgICAgIHBvc3RfZGF0YS5jbGllbnRfaWQgPSAkKHRoaXMpLmRhdGEoJ2NsaWVudCcpXG4vLyAgICAgICQuYWpheCh7XG4vLyAgICAgICAgIHVybDpiYXNlX3VybCsndHJhY2tpbmcvbWVtYmVyLXNlcnZpY2UvJyxcbi8vICAgICAgICAgZGF0YTpwb3N0X2RhdGEsXG4vLyAgICAgICAgIHR5cGU6J1BPU1QnLFxuLy8gICAgICAgICBkYXRhVHlwZTonanNvbicsXG4vLyAgICAgICAgIHN1Y2Nlc3M6ZnVuY3Rpb24oZGF0YSlcbi8vICAgICAgICAge1xuLy8gICAgICAgICAgICAgdGQgPSAnJztcbi8vICAgICAgICAgICAgIGlmKGRhdGEuc3RhdHVzID09ICdzdWNjZXNzJylcbi8vICAgICAgICAgICAgIHtcbi8vICAgICAgICAgICAgICAgICBmb3IoaT0wOyBpPCBkYXRhLm5vZGVzLmxlbmd0aDsgaSsrKVxuLy8gICAgICAgICAgICAgICAgIHtcbi8vICAgICAgICAgICAgICAgICAgICAgdGQgKz0gJzx0cj4nO1xuLy8gICAgICAgICAgICAgICAgICAgICB0ZCArPSAnPHRkPicgKyBkYXRhLm5vZGVzW2ldLnNlcnZpY2VEYXRlICsgJzwvdGQ+Jztcbi8vICAgICAgICAgICAgICAgICAgICAgdGQgKz0gJzx0ZD4nICsgZGF0YS5ub2Rlc1tpXS50cmFja2luZyArICc8L3RkPic7XG4vLyAgICAgICAgICAgICAgICAgICAgIHRkICs9ICc8dGQ+JyArIGRhdGEubm9kZXNbaV0uZGV0YWlsICsgJzwvdGQ+Jztcbi8vICAgICAgICAgICAgICAgICAgICAgdGQgKz0gJzx0ZD4nICsgbnVtYmVyV2l0aENvbW1hcyhkYXRhLm5vZGVzW2ldLmFtb3VudCApKyAnL0Nvc3Q8L3RkPic7XG4vLyAgICAgICAgICAgICAgICAgICAgIHRkICs9ICc8dGQ+JyArIG51bWJlcldpdGhDb21tYXMoZGF0YS5ub2Rlc1tpXS5jaGFyZ2UgKSsgJy9TRjwvdGQ+Jztcbi8vICAgICAgICAgICAgICAgICAgICAgdGQgKz0gJzx0ZD4nICsgbnVtYmVyV2l0aENvbW1hcyhkYXRhLm5vZGVzW2ldLmNoYXJnZTIgKSsgJy9BQzwvdGQ+Jztcbi8vICAgICAgICAgICAgICAgICAgICAgdGQgKz0gJzwvdHI+Jztcbi8vICAgICAgICAgICAgICAgICB9XG4vLyAgICAgICAgICAgICB9XG4vLyAgICAgICAgICAgICAkKCcjZ3JvdXBNb2RhbCB0Ym9keScpLmh0bWwodGQpO1xuLy8gICAgICAgICAgICAgJCgnI2dyb3VwTW9kYWwnKS5tb2RhbCgndG9nZ2xlJyk7XG4vLyAgICAgICAgIH1cbi8vICAgICAgfSlcblxuLy8gfSlcblxuLy8gJCgnI2J0bkJhY2tUcmFja2luZycpLmNsaWNrKGZ1bmN0aW9uKCl7XG4vLyAgICAgc2hvd1RhYignI3RyYWNraW5nX2NvbnRhaW5lcicpO1xuLy8gfSlcblxuXG5cblxuPC9zY3JpcHQ+XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIFRyYWNrTnVtYmVyLnZ1ZT81N2NiZjQ0YyIsInZhciBDb21wb25lbnQgPSByZXF1aXJlKFwiIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9jb21wb25lbnQtbm9ybWFsaXplclwiKShcbiAgLyogc2NyaXB0ICovXG4gIHJlcXVpcmUoXCIhIWJhYmVsLWxvYWRlcj97XFxcImNhY2hlRGlyZWN0b3J5XFxcIjp0cnVlLFxcXCJwcmVzZXRzXFxcIjpbW1xcXCJlbnZcXFwiLHtcXFwibW9kdWxlc1xcXCI6ZmFsc2UsXFxcInRhcmdldHNcXFwiOntcXFwiYnJvd3NlcnNcXFwiOltcXFwiPiAyJVxcXCJdLFxcXCJ1Z2xpZnlcXFwiOnRydWV9fV1dfSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT1zY3JpcHQmaW5kZXg9MCEuL01vZGFsLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtZmIzNGQ0Y2FcXFwifSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vTW9kYWwudnVlXCIpLFxuICAvKiBzY29wZUlkICovXG4gIG51bGwsXG4gIC8qIGNzc01vZHVsZXMgKi9cbiAgbnVsbFxuKVxuQ29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCIvVXNlcnMvam9zaHVhZG9sb3IvRG9jdW1lbnRzL1Byb2plY3RzL1RyYWNrZXIvcmVzb3VyY2VzL2pzL2NvbXBvbmVudHMvTW9kYWwudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gTW9kYWwudnVlOiBmdW5jdGlvbmFsIGNvbXBvbmVudHMgYXJlIG5vdCBzdXBwb3J0ZWQgd2l0aCB0ZW1wbGF0ZXMsIHRoZXkgc2hvdWxkIHVzZSByZW5kZXIgZnVuY3Rpb25zLlwiKX1cblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHsoZnVuY3Rpb24gKCkge1xuICB2YXIgaG90QVBJID0gcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKVxuICBob3RBUEkuaW5zdGFsbChyZXF1aXJlKFwidnVlXCIpLCBmYWxzZSlcbiAgaWYgKCFob3RBUEkuY29tcGF0aWJsZSkgcmV0dXJuXG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICBob3RBUEkuY3JlYXRlUmVjb3JkKFwiZGF0YS12LWZiMzRkNGNhXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9IGVsc2Uge1xuICAgIGhvdEFQSS5yZWxvYWQoXCJkYXRhLXYtZmIzNGQ0Y2FcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH1cbn0pKCl9XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9uZW50LmV4cG9ydHNcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vcmVzb3VyY2VzL2pzL2NvbXBvbmVudHMvTW9kYWwudnVlXG4vLyBtb2R1bGUgaWQgPSA1MlxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9BYm91dC52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTBlOWMwMmViXFxcIn0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL0Fib3V0LnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL1VzZXJzL2pvc2h1YWRvbG9yL0RvY3VtZW50cy9Qcm9qZWN0cy9UcmFja2VyL3Jlc291cmNlcy9qcy9wYWdlcy9BYm91dC52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBBYm91dC52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtMGU5YzAyZWJcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi0wZTljMDJlYlwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvanMvcGFnZXMvQWJvdXQudnVlXG4vLyBtb2R1bGUgaWQgPSA1M1xuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9Db250YWN0TnVtYmVyLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMjU2MzJlZTdcXFwifSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vQ29udGFjdE51bWJlci52dWVcIiksXG4gIC8qIHNjb3BlSWQgKi9cbiAgbnVsbCxcbiAgLyogY3NzTW9kdWxlcyAqL1xuICBudWxsXG4pXG5Db21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcIi9Vc2Vycy9qb3NodWFkb2xvci9Eb2N1bWVudHMvUHJvamVjdHMvVHJhY2tlci9yZXNvdXJjZXMvanMvcGFnZXMvQ29udGFjdE51bWJlci52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBDb250YWN0TnVtYmVyLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0yNTYzMmVlN1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTI1NjMyZWU3XCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9qcy9wYWdlcy9Db250YWN0TnVtYmVyLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNTRcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwidmFyIENvbXBvbmVudCA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2NvbXBvbmVudC1ub3JtYWxpemVyXCIpKFxuICAvKiBzY3JpcHQgKi9cbiAgcmVxdWlyZShcIiEhYmFiZWwtbG9hZGVyP3tcXFwiY2FjaGVEaXJlY3RvcnlcXFwiOnRydWUsXFxcInByZXNldHNcXFwiOltbXFxcImVudlxcXCIse1xcXCJtb2R1bGVzXFxcIjpmYWxzZSxcXFwidGFyZ2V0c1xcXCI6e1xcXCJicm93c2Vyc1xcXCI6W1xcXCI+IDIlXFxcIl0sXFxcInVnbGlmeVxcXCI6dHJ1ZX19XV19IS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvcj90eXBlPXNjcmlwdCZpbmRleD0wIS4vQ29udGFjdFVzLnZ1ZVwiKSxcbiAgLyogdGVtcGxhdGUgKi9cbiAgcmVxdWlyZShcIiEhLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyL2luZGV4P3tcXFwiaWRcXFwiOlxcXCJkYXRhLXYtMjI5NjYyYmNcXFwifSEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvc2VsZWN0b3I/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vQ29udGFjdFVzLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL1VzZXJzL2pvc2h1YWRvbG9yL0RvY3VtZW50cy9Qcm9qZWN0cy9UcmFja2VyL3Jlc291cmNlcy9qcy9wYWdlcy9Db250YWN0VXMudnVlXCJcbmlmIChDb21wb25lbnQuZXNNb2R1bGUgJiYgT2JqZWN0LmtleXMoQ29tcG9uZW50LmVzTW9kdWxlKS5zb21lKGZ1bmN0aW9uIChrZXkpIHtyZXR1cm4ga2V5ICE9PSBcImRlZmF1bHRcIiAmJiBrZXkgIT09IFwiX19lc01vZHVsZVwifSkpIHtjb25zb2xlLmVycm9yKFwibmFtZWQgZXhwb3J0cyBhcmUgbm90IHN1cHBvcnRlZCBpbiAqLnZ1ZSBmaWxlcy5cIil9XG5pZiAoQ29tcG9uZW50Lm9wdGlvbnMuZnVuY3Rpb25hbCkge2NvbnNvbGUuZXJyb3IoXCJbdnVlLWxvYWRlcl0gQ29udGFjdFVzLnZ1ZTogZnVuY3Rpb25hbCBjb21wb25lbnRzIGFyZSBub3Qgc3VwcG9ydGVkIHdpdGggdGVtcGxhdGVzLCB0aGV5IHNob3VsZCB1c2UgcmVuZGVyIGZ1bmN0aW9ucy5cIil9XG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7KGZ1bmN0aW9uICgpIHtcbiAgdmFyIGhvdEFQSSA9IHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIilcbiAgaG90QVBJLmluc3RhbGwocmVxdWlyZShcInZ1ZVwiKSwgZmFsc2UpXG4gIGlmICghaG90QVBJLmNvbXBhdGlibGUpIHJldHVyblxuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgaG90QVBJLmNyZWF0ZVJlY29yZChcImRhdGEtdi0yMjk2NjJiY1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfSBlbHNlIHtcbiAgICBob3RBUEkucmVsb2FkKFwiZGF0YS12LTIyOTY2MmJjXCIsIENvbXBvbmVudC5vcHRpb25zKVxuICB9XG59KSgpfVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbXBvbmVudC5leHBvcnRzXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9qcy9wYWdlcy9Db250YWN0VXMudnVlXG4vLyBtb2R1bGUgaWQgPSA1NVxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9TZXJ2aWNlcy52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTM3NGI5YWYwXFxcIn0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL1NlcnZpY2VzLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL1VzZXJzL2pvc2h1YWRvbG9yL0RvY3VtZW50cy9Qcm9qZWN0cy9UcmFja2VyL3Jlc291cmNlcy9qcy9wYWdlcy9TZXJ2aWNlcy52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBTZXJ2aWNlcy52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtMzc0YjlhZjBcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi0zNzRiOWFmMFwiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvanMvcGFnZXMvU2VydmljZXMudnVlXG4vLyBtb2R1bGUgaWQgPSA1NlxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJ2YXIgQ29tcG9uZW50ID0gcmVxdWlyZShcIiEuLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvY29tcG9uZW50LW5vcm1hbGl6ZXJcIikoXG4gIC8qIHNjcmlwdCAqL1xuICByZXF1aXJlKFwiISFiYWJlbC1sb2FkZXI/e1xcXCJjYWNoZURpcmVjdG9yeVxcXCI6dHJ1ZSxcXFwicHJlc2V0c1xcXCI6W1tcXFwiZW52XFxcIix7XFxcIm1vZHVsZXNcXFwiOmZhbHNlLFxcXCJ0YXJnZXRzXFxcIjp7XFxcImJyb3dzZXJzXFxcIjpbXFxcIj4gMiVcXFwiXSxcXFwidWdsaWZ5XFxcIjp0cnVlfX1dXX0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9c2NyaXB0JmluZGV4PTAhLi9UcmFja051bWJlci52dWVcIiksXG4gIC8qIHRlbXBsYXRlICovXG4gIHJlcXVpcmUoXCIhIS4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlci9pbmRleD97XFxcImlkXFxcIjpcXFwiZGF0YS12LTcyN2I0ODVjXFxcIn0hLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL1RyYWNrTnVtYmVyLnZ1ZVwiKSxcbiAgLyogc2NvcGVJZCAqL1xuICBudWxsLFxuICAvKiBjc3NNb2R1bGVzICovXG4gIG51bGxcbilcbkNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwiL1VzZXJzL2pvc2h1YWRvbG9yL0RvY3VtZW50cy9Qcm9qZWN0cy9UcmFja2VyL3Jlc291cmNlcy9qcy9wYWdlcy9UcmFja051bWJlci52dWVcIlxuaWYgKENvbXBvbmVudC5lc01vZHVsZSAmJiBPYmplY3Qua2V5cyhDb21wb25lbnQuZXNNb2R1bGUpLnNvbWUoZnVuY3Rpb24gKGtleSkge3JldHVybiBrZXkgIT09IFwiZGVmYXVsdFwiICYmIGtleSAhPT0gXCJfX2VzTW9kdWxlXCJ9KSkge2NvbnNvbGUuZXJyb3IoXCJuYW1lZCBleHBvcnRzIGFyZSBub3Qgc3VwcG9ydGVkIGluICoudnVlIGZpbGVzLlwiKX1cbmlmIChDb21wb25lbnQub3B0aW9ucy5mdW5jdGlvbmFsKSB7Y29uc29sZS5lcnJvcihcIlt2dWUtbG9hZGVyXSBUcmFja051bWJlci52dWU6IGZ1bmN0aW9uYWwgY29tcG9uZW50cyBhcmUgbm90IHN1cHBvcnRlZCB3aXRoIHRlbXBsYXRlcywgdGhleSBzaG91bGQgdXNlIHJlbmRlciBmdW5jdGlvbnMuXCIpfVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkgeyhmdW5jdGlvbiAoKSB7XG4gIHZhciBob3RBUEkgPSByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpXG4gIGhvdEFQSS5pbnN0YWxsKHJlcXVpcmUoXCJ2dWVcIiksIGZhbHNlKVxuICBpZiAoIWhvdEFQSS5jb21wYXRpYmxlKSByZXR1cm5cbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAoIW1vZHVsZS5ob3QuZGF0YSkge1xuICAgIGhvdEFQSS5jcmVhdGVSZWNvcmQoXCJkYXRhLXYtNzI3YjQ4NWNcIiwgQ29tcG9uZW50Lm9wdGlvbnMpXG4gIH0gZWxzZSB7XG4gICAgaG90QVBJLnJlbG9hZChcImRhdGEtdi03MjdiNDg1Y1wiLCBDb21wb25lbnQub3B0aW9ucylcbiAgfVxufSkoKX1cblxubW9kdWxlLmV4cG9ydHMgPSBDb21wb25lbnQuZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvanMvcGFnZXMvVHJhY2tOdW1iZXIudnVlXG4vLyBtb2R1bGUgaWQgPSA1N1xuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX3ZtLl9tKDApXG59LHN0YXRpY1JlbmRlckZuczogW2Z1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcImFib3V0LXBhZ2VcIlxuICAgIH1cbiAgfSwgW19jKCd0YWJsZScsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0YWJsZVwiXG4gIH0sIFtfYygndHInLCB7XG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogXCJ0cmFuc3BhcmVuY3lcIlxuICAgIH1cbiAgfSwgW19jKCd0ZCcsIFtfYygnaDMnLCBbX3ZtLl92KFwiVHJhbnNwYXJlbmN5XCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdwJywgW192bS5fdihcIldlIHVuZGVydGFrZSB0byBiZSB0cmFuc3BhcmVudCBpbiBhbGwgb3VyIHByb2Zlc3Npb25hbCBjb250YWN0cy4gSW4gcHJhY3RpY2UsIHRoaXMgbWVhbnMgd2Ugd2lsbCBpZGVudGlmeSBvdXIgY2xpZW50IGluIHByb2FjdGl2ZSBleHRlcm5hbCBwcm9mZXNzaW9uYWxcXG5cXHQgICAgICAgICAgICBjb250YWN0cyBhbmQgc2VydmljZXMgb2ZmZXJlZCBmb3IgYSBjbGllbnQuXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdwJywgW192bS5fdihcIlNvbWV0aW1lcyB3ZSBlc3RhYmxpc2ggY29hbGl0aW9ucyBvbiBiZWhhbGYgb2YgY2xpZW50cyB0byB3b3JrIG9uIHNwZWNpZmljIGlzc3Vlcy4gV2hlbmV2ZXIgd2UgZG8gc28sIHdlIHVuZGVydGFrZSB0byBiZSB0cmFuc3BhcmVudCBhYm91dCBcXG5cXHQgICAgICAgICAgICB0aGUgcHVycG9zZSBvZiB0aGUgY29hbGl0aW9uIGFuZCBwcmluY2lwYWwgZnVuZGVycy5cIildKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0cicsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcImhvbmVzdHlcIlxuICAgIH1cbiAgfSwgW19jKCd0ZCcsIFtfYygnaDMnLCBbX3ZtLl92KFwiSG9uZXN0eVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygncCcsIFtfdm0uX3YoXCJXZSB1bmRlcnRha2Ugbm90IHRvIGRpc2Nsb3NlIGFueSBjb25maWRlbnRpYWwgaW5mb3JtYXRpb24gcHJvdmlkZWQgdG8gdXMgaW4gdGhlIGNvdXJzZSBvZiBvdXIgd29yayBmb3IgY2xpZW50cywgdW5sZXNzIGV4cGxpY2l0eSBhdXRob3JpemVkIHRvIGRvXFxuXFx0ICAgICAgICAgICAgc28gb3IgcmVxdWlyZWQgYnkgZ292ZXJubWVudHMgb3Igb3RoZXIgbGVnYWwgYXV0aG9yaXRpZXMuIEFsbCBXWUMgZW1wbG95ZWVzIGFyZSBjb250cmFjdHVhbGx5IG9ibGlndWVkIHRvIHJlc3BlY3QgdGhpcyB1bmRlcnRha2luZyBvblxcblxcdCAgICAgICAgICAgIGFsbCBpbnRlcm5hbCBtYXR0ZXJzLlwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygncCcsIFtfdm0uX3YoXCJXZSBzdG9yZSBhbmQgaGFuZGxlIGNvbmZpZGVudGlhbCBpbmZvcm1hdGlvbiBzbyBhcyB0byBlbnN1cmUgdGhhdCBvbmx5IHRob3NlIHdobyBhcmUgd29ya2luZyB3aXRoIHRoZSBpbmZvcm1hdGlvbiBoYXZlIGFjY2VzcyB0byBpdC5cIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3AnLCBbX3ZtLl92KFwiV2UgYWR2aWNlIG91ciBjbGllbnRzIHRvIHVwaG9sZCBhbGwgdGhlc2UgdmFsdWVzIGluIHRoZWlyIG93biByZWxhdGVkIHRvIHRoZSBhZHZpY2Ugd2UgcHJvdmlkZSB0aGVtLlwiKV0pXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3RyJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwiY29uZmlkZW50aWFsaXR5XCJcbiAgICB9XG4gIH0sIFtfYygndGQnLCBbX2MoJ2gzJywgW192bS5fdihcIkNvbmZpZGVudGlhbGl0eVwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygncCcsIFtfdm0uX3YoXCJXZSB1bmRlcnN0YW5kIHRvIGJlIGhvbmVzdCBpbiBhbGwgb3VyIHByb2Zlc3Npb25hbCBkZWFsaW5ncy4gV2UgdW5kZXJ0YWtlIG5ldmVyIGtub3dpbmdseSB0byBzcHJlYWQgZmFsc2Ugb3IgbWlzbGVhZGluZyBpbmZvcm1hdGlvbiBhbmQgdG8gdGFrZSBcXG5cXHQgICAgICAgICAgICByZWFzb25hYmxlIGNhcmUgdG8gYXZvaWQgZG9pbmcgc28gaW5hZHZlcnRlbnRseS5cIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3AnLCBbX3ZtLl92KFwiV2UgYWxzbyBhc2sgb3VyIGNsaWVudHMgdG8gYmUgaG9uZXN0IHdpdGggdXMgYW5kIG5vdCB0byByZXF1ZXN0IHRoYXQgd2UgY29tcHJvbWlzZWQgb3VyIHByaW5jaXBsZXMgb3IgdGhlIGxhdy5cIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3AnLCBbX3ZtLl92KFwiV2UgdGFrZSBldmVyeSBwb3NzaWJsZSBzdGVwIHRvIGF2b2lkIGNvbmZsaWN0cyBvZiBpbnRlcmVzdC5cIildKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCd0cicsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcImludGVncml0eVwiXG4gICAgfVxuICB9LCBbX2MoJ3RkJywgW19jKCdoMycsIFtfdm0uX3YoXCJJbnRlZ3JpdHlcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ3AnLCBbX3ZtLl92KFwiV2Ugd2lsbCBvbmx5IHdvcmsgZm9yIHRoZSBvcmdhbml6YXRpb25zIHRoYXQgYXJlIHByZXBhcmVkIHRvIHVwaG9sZCB0aGUgdmFsdWVzIG9mIHRyYW5zcGFyZW5jeSBhbmQgaG9uZXN0eSBpbiB0aGUgY291cnNlIG9mIG91ciB3b3JrIGZvciB0aGVtLlwiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygncCcsIFtfdm0uX3YoXCJTb21ldGltZXMgb3VyIGNsaWVudHMgYXJlIGludm9sdmVkIHdpdGggY29udGVudGl1b3MgaXNzdWVzLiBJdCBpcyBwYXJ0IG9mIG91ciBqb2IgdG8gcHJvdmlkZSBjb21tdW5pY2F0aW9ucyBhc3Npc3RhbmNlIHRvIHRob3NlIGNsaWVudHMuIFxcblxcdCAgICAgICAgICAgIFdlIHVuZGVydGFrZSB0byByZXByZXNlbnQgdGhvc2UgY2xpZW50cyBvbmx5IGluIHdheXMgY29uc2lzdGVudCB3aXRoIHRoZSB2YWx1ZXMgb2YgaG9uZXN0eSBhbmQgdHJhbnNwYXJlbmN5IGJvdGggaW4gd2hhdCB0aGV5IGRvIGFuZCBcXG5cXHQgICAgICAgICAgICBpbiB3aGF0IHdlIGRvIGZvciB0aGVtLlwiKV0pXSldKV0pXSlcbn1dfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi0wZTljMDJlYlwiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTBlOWMwMmViXCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvanMvcGFnZXMvQWJvdXQudnVlXG4vLyBtb2R1bGUgaWQgPSA1OFxuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCJtb2R1bGUuZXhwb3J0cz17cmVuZGVyOmZ1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX3ZtLl9tKDApXG59LHN0YXRpY1JlbmRlckZuczogW2Z1bmN0aW9uICgpe3ZhciBfdm09dGhpczt2YXIgX2g9X3ZtLiRjcmVhdGVFbGVtZW50O3ZhciBfYz1fdm0uX3NlbGYuX2N8fF9oO1xuICByZXR1cm4gX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjb2wteHMtMTJcIlxuICB9LCBbX2MoJ2gzJywgW19jKCdiJywgW192bS5fdihcIiBBZGRyZXNzIFwiKV0pLCBfYygnYnInKSwgX2MoJ3NtYWxsJywgW192bS5fdihcIiBVMTEwIEJhbGFndGFzIFN0LCBCYWxhZ3RhcyBWaWxsYXMgQmFyYW5nYXkgMTUgU2FuIElzaWRybyBQYXNheSBDaXR5IFwiKV0pXSksIF92bS5fdihcIiBcIiksIF9jKCdoMycsIFtfYygnYicsIFtfdm0uX3YoXCIgQ29udGFjdCBcIildKSwgX2MoJ2JyJyksIF9jKCdzbWFsbCcsIFtfdm0uX3YoXCIgKDAyKSAzNTQgODAgMjEgXCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2gzJywgW19jKCdiJywgW192bS5fdihcIiBPcGVyYXRpbmcgaG91cnNcIildKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2JyJyksIF9jKCdzbWFsbCcsIFtfdm0uX3YoXCIgTW9uZGF5IC0gRnJpZGF5ICg4YW0gLSA1cG0pXCIpXSldKV0pXG59XX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtMjI5NjYyYmNcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0yMjk2NjJiY1wifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2pzL3BhZ2VzL0NvbnRhY3RVcy52dWVcbi8vIG1vZHVsZSBpZCA9IDU5XG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfdm0uX20oMClcbn0sc3RhdGljUmVuZGVyRm5zOiBbZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbC14cy0xMlwiXG4gIH0sIFtfYygnaDMnLCBbX2MoJ2InLCBbX3ZtLl92KFwiIEFkZHJlc3MgXCIpXSksIF9jKCdicicpLCBfYygnc21hbGwnLCBbX3ZtLl92KFwiIFUxMTAgQmFsYWd0YXMgU3QsIEJhbGFndGFzIFZpbGxhcyBCYXJhbmdheSAxNSBTYW4gSXNpZHJvIFBhc2F5IENpdHkgXCIpXSldKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2gzJywgW19jKCdiJywgW192bS5fdihcIiBDb250YWN0IFwiKV0pLCBfYygnYnInKSwgX2MoJ3NtYWxsJywgW192bS5fdihcIiAoMDIpIDM1NCA4MCAyMSBcIildKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnaDMnLCBbX2MoJ2InLCBbX3ZtLl92KFwiIE9wZXJhdGluZyBob3Vyc1wiKV0pLCBfdm0uX3YoXCIgXCIpLCBfYygnYnInKSwgX2MoJ3NtYWxsJywgW192bS5fdihcIiBNb25kYXkgLSBGcmlkYXkgKDhhbSAtIDVwbSlcIildKV0pXSlcbn1dfVxubW9kdWxlLmV4cG9ydHMucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5pZiAobW9kdWxlLmhvdCkge1xuICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gIGlmIChtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgcmVxdWlyZShcInZ1ZS1ob3QtcmVsb2FkLWFwaVwiKS5yZXJlbmRlcihcImRhdGEtdi0yNTYzMmVlN1wiLCBtb2R1bGUuZXhwb3J0cylcbiAgfVxufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vfi92dWUtbG9hZGVyL2xpYi90ZW1wbGF0ZS1jb21waWxlcj97XCJpZFwiOlwiZGF0YS12LTI1NjMyZWU3XCJ9IS4vfi92dWUtbG9hZGVyL2xpYi9zZWxlY3Rvci5qcz90eXBlPXRlbXBsYXRlJmluZGV4PTAhLi9yZXNvdXJjZXMvanMvcGFnZXMvQ29udGFjdE51bWJlci52dWVcbi8vIG1vZHVsZSBpZCA9IDYwXG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsIm1vZHVsZS5leHBvcnRzPXtyZW5kZXI6ZnVuY3Rpb24gKCl7dmFyIF92bT10aGlzO3ZhciBfaD1fdm0uJGNyZWF0ZUVsZW1lbnQ7dmFyIF9jPV92bS5fc2VsZi5fY3x8X2g7XG4gIHJldHVybiBfYygnZGl2Jywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwic2VydmljZXMtcGFnZXNcIlxuICAgIH1cbiAgfSwgW192bS5fbCgoX3ZtLnNlcnZpY2VzKSwgZnVuY3Rpb24oc2VydmljZSkge1xuICAgIHJldHVybiBfYygnYnV0dG9uJywge1xuICAgICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1saXN0XCIsXG4gICAgICBvbjoge1xuICAgICAgICBcImNsaWNrXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgIF92bS5zaG93RGVzY3JpcHRpb24oc2VydmljZS5pZClcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sIFtfdm0uX3YoXCJcXG4gICAgICAgIFwiICsgX3ZtLl9zKHNlcnZpY2UudGl0bGUpICsgXCJcXG4gICAgXCIpXSlcbiAgfSksIF92bS5fdihcIiBcIiksIF9jKCdtb2RhbCcsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJpZFwiOiBcInNlcnZpY2VNb2RhbFwiXG4gICAgfVxuICB9LCBbX2MoJ3RlbXBsYXRlJywge1xuICAgIGF0dHJzOiB7XG4gICAgICBcInNsb3RcIjogXCJtb2RhbC10aXRsZVwiXG4gICAgfSxcbiAgICBzbG90OiBcIm1vZGFsLXRpdGxlXCJcbiAgfSwgW192bS5fdihcIk1vZGFsIFRpdGxlXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCd0ZW1wbGF0ZScsIHtcbiAgICBhdHRyczoge1xuICAgICAgXCJzbG90XCI6IFwibW9kYWwtYm9keVwiXG4gICAgfSxcbiAgICBzbG90OiBcIm1vZGFsLWJvZHlcIlxuICB9LCBbX3ZtLl92KFwiTW9kYWwgVGl0bGVcIildKV0sIDIpXSwgMilcbn0sc3RhdGljUmVuZGVyRm5zOiBbXX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtMzc0YjlhZjBcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi0zNzRiOWFmMFwifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2pzL3BhZ2VzL1NlcnZpY2VzLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNjFcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTEyXCJcbiAgfSwgW192bS5fbSgwKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJjYXJkXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiY2FyZC1ib2R5XCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cCBmb3JtLWdyb3VwLWxnICBsYWJlbC1mbG9hdGluZ1wiLFxuICAgIGNsYXNzOiB7XG4gICAgICAnaGFzLWVycm9yJzogX3ZtLmZvcm0uZXJyb3JzLmhhcygncGFja2FnZScpXG4gICAgfVxuICB9LCBbX2MoJ2xhYmVsJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImNvbnRyb2wtbGFiZWxcIlxuICB9LCBbX3ZtLl92KFwiVHJhY2tpbmcgTnVtYmVyXCIpXSksIF92bS5fdihcIiBcIiksIF9jKCdpbnB1dCcsIHtcbiAgICBkaXJlY3RpdmVzOiBbe1xuICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICB2YWx1ZTogKF92bS5mb3JtLnBhY2thZ2UpLFxuICAgICAgZXhwcmVzc2lvbjogXCJmb3JtLnBhY2thZ2VcIlxuICAgIH1dLFxuICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwidHJhY2tpbmdOdW1iZXJcIlxuICAgIH0sXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidmFsdWVcIjogKF92bS5mb3JtLnBhY2thZ2UpXG4gICAgfSxcbiAgICBvbjoge1xuICAgICAgXCJpbnB1dFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7IHJldHVybjsgfVxuICAgICAgICBfdm0uZm9ybS5wYWNrYWdlID0gJGV2ZW50LnRhcmdldC52YWx1ZVxuICAgICAgfVxuICAgIH1cbiAgfSldKSwgX3ZtLl92KFwiIFwiKSwgKF92bS5mb3JtLmVycm9ycy5oYXMoJ3BhY2thZ2UnKSkgPyBfYygncCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWRhbmdlciBoZWxwLWJsb2NrXCIsXG4gICAgZG9tUHJvcHM6IHtcbiAgICAgIFwidGV4dENvbnRlbnRcIjogX3ZtLl9zKF92bS5mb3JtLmVycm9ycy5nZXQoJ3BhY2thZ2UnKSlcbiAgICB9XG4gIH0pIDogX3ZtLl9lKCksIF92bS5fdihcIiBcIiksIF9jKCdjZW50ZXInLCBbX2MoJ2J1dHRvbicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlciBidG4gYnRuLWxnIGJ0bi1yYWlzZWQgYnRuLXByaW1hcnlcIixcbiAgICBvbjoge1xuICAgICAgXCJjbGlja1wiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgX3ZtLnN1Ym1pdCgpXG4gICAgICB9XG4gICAgfVxuICB9LCBbX3ZtLl92KFwiU3VibWl0XCIpXSldKV0sIDEpXSldKVxufSxzdGF0aWNSZW5kZXJGbnM6IFtmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdoMycsIFtfdm0uX3YoXCJUcmFja2luZyBOdW1iZXJcXG4gICAgICAgIFxcdFwiKSwgX2MoJ2JyJyksIF92bS5fdihcIiBcIiksIF9jKCdzbWFsbCcsIFtfdm0uX3YoXCJNb25pdG9yIHlvdXIgcGFja2FnZSBieSBlbnRlcmluZyB0aGUgdHJhY2tpbmcgbnVtYmVyIGdpdmVuIGJ5IFdZQyBCdXNpbmVzcyBDb25zdWx0YW5jeS5cIildKV0pXG59XX1cbm1vZHVsZS5leHBvcnRzLnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuaWYgKG1vZHVsZS5ob3QpIHtcbiAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICBpZiAobW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgIHJlcXVpcmUoXCJ2dWUtaG90LXJlbG9hZC1hcGlcIikucmVyZW5kZXIoXCJkYXRhLXYtNzI3YjQ4NWNcIiwgbW9kdWxlLmV4cG9ydHMpXG4gIH1cbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL34vdnVlLWxvYWRlci9saWIvdGVtcGxhdGUtY29tcGlsZXI/e1wiaWRcIjpcImRhdGEtdi03MjdiNDg1Y1wifSEuL34vdnVlLWxvYWRlci9saWIvc2VsZWN0b3IuanM/dHlwZT10ZW1wbGF0ZSZpbmRleD0wIS4vcmVzb3VyY2VzL2pzL3BhZ2VzL1RyYWNrTnVtYmVyLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNjJcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwibW9kdWxlLmV4cG9ydHM9e3JlbmRlcjpmdW5jdGlvbiAoKXt2YXIgX3ZtPXRoaXM7dmFyIF9oPV92bS4kY3JlYXRlRWxlbWVudDt2YXIgX2M9X3ZtLl9zZWxmLl9jfHxfaDtcbiAgcmV0dXJuIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwgXCIsXG4gICAgYXR0cnM6IHtcbiAgICAgIFwiaWRcIjogX3ZtLmlkLFxuICAgICAgXCJ0YWJpbmRleFwiOiBcIi0xXCIsXG4gICAgICBcInJvbGVcIjogXCJkaWFsb2dcIixcbiAgICAgIFwiYXJpYS1sYWJlbGxlZGJ5XCI6IFwid3ljLW1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWRpYWxvZ1wiLFxuICAgIGNsYXNzOiBfdm0ubW9kYWxTaXplXG4gIH0sIFtfYygnZGl2Jywge1xuICAgIHN0YXRpY0NsYXNzOiBcIm1vZGFsLWNvbnRlbnQgYW5pbWF0ZWQgZmFkZUluXCJcbiAgfSwgW19jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtaGVhZGVyXCJcbiAgfSwgW19jKCdoNCcsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC10aXRsZVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcImlkXCI6IFwid3ljLW1vZGFsLWxhYmVsXCJcbiAgICB9XG4gIH0sIFtfdm0uX3QoXCJtb2RhbC10aXRsZVwiLCBbX3ZtLl92KFwiTW9kYWwgVGl0bGVcIildKV0sIDIpXSksIF92bS5fdihcIiBcIiksIF9jKCdkaXYnLCB7XG4gICAgc3RhdGljQ2xhc3M6IFwibW9kYWwtYm9keVwiXG4gIH0sIFtfdm0uX3QoXCJtb2RhbC1ib2R5XCIsIFtfdm0uX3YoXCJNb2RhbCBCb2R5XCIpXSldLCAyKSwgX3ZtLl92KFwiIFwiKSwgX2MoJ2RpdicsIHtcbiAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1mb290ZXJcIlxuICB9LCBbX3ZtLl90KFwibW9kYWwtZm9vdGVyXCIsIFtfYygnYnV0dG9uJywge1xuICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeVwiLFxuICAgIGF0dHJzOiB7XG4gICAgICBcInR5cGVcIjogXCJidXR0b25cIixcbiAgICAgIFwiZGF0YS1kaXNtaXNzXCI6IFwibW9kYWxcIlxuICAgIH1cbiAgfSwgW192bS5fdihcIkNsb3NlXCIpXSldKV0sIDIpXSldKV0pXG59LHN0YXRpY1JlbmRlckZuczogW119XG5tb2R1bGUuZXhwb3J0cy5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcbmlmIChtb2R1bGUuaG90KSB7XG4gIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgaWYgKG1vZHVsZS5ob3QuZGF0YSkge1xuICAgICByZXF1aXJlKFwidnVlLWhvdC1yZWxvYWQtYXBpXCIpLnJlcmVuZGVyKFwiZGF0YS12LWZiMzRkNGNhXCIsIG1vZHVsZS5leHBvcnRzKVxuICB9XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9+L3Z1ZS1sb2FkZXIvbGliL3RlbXBsYXRlLWNvbXBpbGVyP3tcImlkXCI6XCJkYXRhLXYtZmIzNGQ0Y2FcIn0hLi9+L3Z1ZS1sb2FkZXIvbGliL3NlbGVjdG9yLmpzP3R5cGU9dGVtcGxhdGUmaW5kZXg9MCEuL3Jlc291cmNlcy9qcy9jb21wb25lbnRzL01vZGFsLnZ1ZVxuLy8gbW9kdWxlIGlkID0gNjNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIl0sInNvdXJjZVJvb3QiOiIifQ==