const routes = [
    { path: '/about', name:'about', component: require('./pages/About.vue') },
    { path: '/contact-us', component: require('./pages/ContactUs.vue')  },
    { path: '/services', component: require('./pages/Services.vue')  },
    { path: '/tracking-number', name:'package-by-tracking', component: require('./pages/TrackNumber.vue')  },
    { path: '/mobile-number',name:'mobile-number', component: require('./pages/ContactNumber.vue')  },
    { path: '/package', name:'package', component: require('./pages/Package.vue'), props: true  },
    { path: '/package-list', name:'package-list', component: require('./pages/PackageList.vue') },
    { path: '/verify-user', name:'verify-user', component: require('./pages/VerifyUser.vue') },
]

const router = new VueRouter({
    routes // short for `routes: routes`
})

//first page to load
router.replace({ path: '/mobile-number', redirect: '/' })
// router.replace({ path: '/tracking-number', redirect: '/' })

// load components
Vue.component('modal', require('./components/Modal.vue'));

// filters
Vue.filter('currency', function (value) {
    return numberWithCommas(parseFloat("0"+value).toFixed(2));
});

Vue.filter('titleCase', function (str) {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
});

Vue.filter('reverse', function(value) {
  // slice to make a copy of array, then reverse the copy
  return value.slice().reverse();
});

window.app = new Vue({
    router,
    
    el: '.app',

    watch:{
        isLoading:function(value){
            if(value){
                //show page loading
                $('.progress').show();
            }else{
                //hide page loading
                $('.progress').hide();
            }
        }
    },

    data:{
        appTitle: 'WYC Group',
        hasBack:false,
        isLoading:false,
        hasConnection:false,
        client:{
            first_name:null,
            last_name:null,
            id:null,
            gender:null,
            mobile_number:null
        },
        packages:null,
        relatedClients:{}
    },

    methods:{
        closeMenu() {
            hideDrawer();
        },
        goBack(){
            if(this.hasBack){
                if(this.$route.name == 'package-list' && this.client.id){
                    router.go(-2);
                }
                else{
                    router.go(-1);
                }
            }else
            {
                showDrawer();
            }
        },
        saveProfile(){
            this._insertTableValues(function(){
                snackbar.show({text:"Profile Saved!"})
            });

        },
        getProfile(){
            this._getTableValues((clientData)=>{
                if(clientData.length){
                    clientData = clientData[0];
                    this.client = clientData;
    
                    this.$router.push({ name: 'package-list'})
                }else
                {
                    this.$router.push({ name: 'mobile-number'})
                }
            })
        },
        logout(){
            this._deleteTableValues((result)=>{
                if(result)
                {
                    this.client.id = null;
                    this.client.mobile_number = null;
                    this.client.first_name = null;
                    this.client.last_name = null;
                    this.client.gender = null;

                    hideDrawer();
                    this.$router.push({ name: 'about'})
                    snackbar.show({text:'Logout Successfully!'})
                }
            })
        },
        _cleanData(str){
            return str.trim().replace("'","");
        },

        //db functions
        _insertTableValues(callback){
            query = "INSERT INTO USER(first_name,last_name,id,gender,mobile_number) VALUES ("
                // + "1,"
                + "'" + this.client.first_name + "',"
                + "'" + this.client.last_name + "',"
                + "'" + this.client.id + "',"
                + "'" + this.client.gender + "',"
                + "'" + this.client.mobile_number + "'"
                +")";

            db.transaction(function(tx) {
                tx.executeSql(query);
            }, function(error) {
                // alert('Transaction ERROR: ' + error.message);
                callback(-1);
            }, function() {
                callback(1);
            });
        },
        _updateTableValues(table,dataVar,id,callback){
            dataString = '';
            for (var prop in dataVar) {
                if (dataVar.hasOwnProperty(prop)) {
                    dataString += prop  +' = '+ '\''+dataVar[prop] + '\',';
                }
            }
            dataString = dataString.replace(/,\s*$/, "");
            query = 'UPDATE '+table+' SET ';
            query += dataString;
            query += ' WHERE id = 1';
            db.transaction(function(tx){
                tx.executeSql(query,[],function(tx, results){
                    callback(results.rowsAffected);
                })
            })
        },
        _getTableValues(callback) {
            var dataRetrieve = [];
            var query = "SELECT * FROM USER";
            db.transaction((tx)=> {
                tx.executeSql(query, [], (tx, labelsResults)=> {
                      for (var x = 0; x < labelsResults.rows.length; x++) {
                        var labelsRow = labelsResults.rows.item(x);
                        dataRetrieve.push(labelsRow);
                        // alert(labelsRow);
                    }               
                    callback(dataRetrieve);
                }, (tx, error)=> {
                  // alert('SELECT error: ' + error.message);
                });
              });

        },
        _deleteTableValues(callback) {
            var dataRetrieve = [];
            var query = "DELETE FROM USER";
            db.transaction(function(tx){
                 tx.executeSql(query, null, function(transaction, result) {
                    callback(result);
                }, function(transaction, error) {
                    console.log(error);
                });
            })
        },
        _toNormal(data){
            return JSON.parse(JSON.stringify(data));
        },
    },


    
})