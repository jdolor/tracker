window.Vue = require('vue');
window.VeeValidate = require('vee-validate');
window.VueRouter = require('vue-router').default;

window.Vue.use(VeeValidate);
window.Vue.use(VueRouter);


window.axios = require('axios');

window.axios.defaults.onUploadProgress = (function(){
    app.isLoading = true;
})

window.snackbar = require('node-snackbar');

