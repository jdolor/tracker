require('./webdb');
require('./bootstrap');

window.Event = require('./core/event.js').default;
window.Errors = require('./core/errors.js').default;
window.Form = require('./core/form.js').default;

$(function() {
    FastClick.attach(document.body);
});

(function(factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'hammerjs'], factory);
    } else if (typeof exports === 'object') {
        factory(require('jquery'), require('hammerjs'));
    } else {
        factory(jQuery, Hammer);
    }
}(function($, Hammer) {
    function hammerify(el, options) {
        var $el = $(el);
        if(!$el.data("hammer")) {
            $el.data("hammer", new Hammer($el[0], options));
        }
    }

    $.fn.hammer = function(options) {
        return this.each(function() {
            hammerify(this, options);
        });
    };

    // extend the emit method to also trigger jQuery events
    Hammer.Manager.prototype.emit = (function(originalEmit) {
        return function(type, data) {
            originalEmit.call(this, type, data);
            $(this.element).trigger({
                type: type,
                gesture: data
            });
        };
    })(Hammer.Manager.prototype.emit);
}));

window.isDrawerVisible = false;

window.numberWithCommas = function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

window.hideDrawer = function(){
    $('#drawer').hide('fast');
    $('#drawer .drawer-content')
        .stop(true,true)
        .animate({'left':'-100%'},'fast');
    window.isDrawerVisible = false;
}
window.showDrawer = function(){
    $('#drawer').show('fast');
    $('#drawer .drawer-content')
        .stop(true,true)
        .animate({'left':'0'},'fast');
    window.isDrawerVisible = true;
}

window.overScrollAnimation = 'bounce';

window.base_url = 'http://topwyc.com/api/';
// window.base_url = 'http://localhost:8080/api/';
window.tracking_number_url = base_url + 'track-number'; // 
window.package_number_url = base_url + 'get-packages-number';
window.package_client_url = base_url+ 'get-packages-client';

jQuery.fn.extend({
    mobileScroll: function (container) {
        $(this).scroll(function(){
            if( $(this).scrollTop() == 0 ){
                console.log('top')
            }else if( $(this).scrollTop() + $(this).height() == $(container).height() )
            {
                console.log('bottom')
            }
        })
    }
});

document.addEventListener("offline", function(){
    app.hasConnection = false;
    // app.isLoading = false;
}, false);

document.addEventListener("online", function(){
    app.hasConnection = true;
    // app.isLoading = false;
}, false);


$(document).ready(function(){
    // app.hasConnection = true;
    
	$.material.init();

    // $('#appContent').mobileScroll('.content-section');

    (function() {
        new Hammer( $( "body" )[ 0 ], {
            domEvents: true
        } );
        $( "body" ).on( "swipeleft", function( e ) {
            if(window.isDrawerVisible)
            {
                hideDrawer();
            }
        } );
        $( "body" ).on( "swiperight", function( e ) {
            showDrawer();
        } );
    } )();

	$('[data-toggle="popover"]').popover();
	$('[data-toggle="tooltip"]').popover();

    $('#drawer .drawer-backdrop').click(function(){
        hideDrawer();
    })
})

document.addEventListener('deviceready', onDeviceReady, false);
