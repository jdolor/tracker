class Form {
    constructor(data = {}, axiosConfig = {}, config = {}) {
        this.originalData = data;
        this.errors = new Errors();
        this.axios = $.isEmptyObject(axiosConfig) ? axios : axios.create(axiosConfig);
        this.config = config;

        for (var field in data) {
            this[field] = data[field];
        }
    }

    data() {
        var data = {};

        for (var property in this.originalData) {
            data[property] = this[property];
        }

        return data;
    }

    reset() {
        for (var property in this.originalData) {
            if (this.config.hasOwnProperty('resetExceptions') && !~this.config.resetExceptions.indexOf(property)) {
                this[property] = '';
            }
        }

        this.errors.clear();
    }

    submit(requestType, url, data) {
        if(!app.hasConnection){
            // no connection
            snackbar.show({text:'No Connection..'})
        }else if(app.isLoading){
            // no connection
            snackbar.show({text:'Please wait..'})
        }else{
            return new Promise((resolve, reject) => {
                this.axios[requestType](url, data || this.data())
                    .then(response => {
                        this._onSuccess(response.data);

                        resolve(response.data);
                    })
                    .catch(error => {
                        alert(JSON.stringify(error));
                        this._onFail(error.response.data);
                        reject(error.response.data);
                    });
            });
        }
    }

    _onSuccess(data) {
        this.removeHasLoading()
        this.reset();
        app.isLoading = false;
    }

    _onFail(errors) {
        this.removeHasLoading()
        this.errors.record(errors);
        app.isLoading = false;
    }

    removeHasLoading(){
        var content = $('body').find('.btn-content');
        var loader = $('body').find('.loader');
        if(loader)
        {
            loader.fadeOut(100,function(){
                content.fadeIn(100);
            });
        }
    }
}

export default Form;