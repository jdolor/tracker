window.db = null

window.onDeviceReady = function (){
	window.sqlitePlugin.selfTest(function() {
		console.log('SELF test OK');
	});

	db = window.sqlitePlugin.openDatabase({name: 'WycGroupTracker.db', location: 'default',createFromLocation:1,createFromResource:1});
	db.transaction(function (tx) {  
		tx.executeSql(
			'CREATE TABLE IF NOT EXISTS USER (sqlite_id INTEGER PRIMARY KEY, id, first_name, last_name, gender, mobile_number)'
		);
	}, function(error) {
        console.log('Transaction ERROR: ' + error.message);
    }, function() {
        app.getProfile();
    });
}
