const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */



mix.sass('resources/sass/app.scss', 'css', {
		includePaths: ['node_modules/bootstrap-sass/assets/stylesheets']
	})
	.sass('resources/sass/sidebar.scss','css')
;

//js
mix.js([
		 'node_modules/bootstrap/dist/js/bootstrap'
		,'node_modules/bootstrap-material-design/scripts/index'

	],'js/bootstrap-material-design.js')
   	.js('resources/js/app.js', 'js')
   	.js('resources/js/index.js', 'js')
	.js('node_modules/jquery-mobile/js/animationComplete.js','js/jquery-mobile.js', {
		includePaths: ['node_modules/jquery-ui/ui/widgets']
	})
;

//copy things
mix.copy('node_modules/bootstrap-material-design/bower_components/jquery/dist/jquery.js','www/js/jquery.js');
mix.copy('node_modules/fastclick/lib/fastclick.js','www/js/fastclick.js');
mix.copy('node_modules/font-awesome/fonts','www/fonts');
mix.copy('node_modules/material-design-icons/iconfont','www/fonts');
mix.copy('node_modules/font-awesome/css/font-awesome.css','www/css/font-awesome.css');
mix.copy('resources/img','www/img');

//remove processing of urls for images and backgrounds
// mix.options({
// 	processCssUrls:false
// });

if (mix.config.inProduction) {
    mix.version();
} else {
    mix.sourceMaps();
}

mix.setPublicPath('www');